<?php
/**
 * Markdown to PDF Doc
 */

chdir( '..' );

session_name( 'RosarioSIS' );

session_start();

$_SESSION['USERNAME'] = 'MarkDownPDF';
$_SESSION['STAFF_ID'] = '-1';

require_once 'Warehouse.php';
require_once 'ProgramFunctions/MarkDownHTML.fnc.php';

if ( empty( $wkhtmltopdfPath ) )
{
	$wkhtmltopdfPath = 'wkhtmltopdf';
}

error_reporting( E_ALL ^ E_NOTICE ^ E_WARNING );

$md_files = array(
	'README.md',
	'INSTALL.md',
	'INSTALL_es.md',
	'INSTALL_fr.md',
);

foreach ( $md_files as $md_file )
{
	MarkdownToPDF( $md_file );
}


function MarkdownToPDF( $md_file )
{
	static $i = 0;

	if ( ! file_exists( $md_file ) )
	{
		return false;
	}

	$md = file_get_contents( $md_file );

	if ( $md_file === 'README.md' )
	{
		// README.md, remove screenshots (reduce PDF size)!
		$md = str_replace(
			[
				'![Calendars](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_calendar_en_2021.png)',

				//'![Student Info](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_en_2021.png)',

				'![Student Schedule](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_scheduling_en_2021.png)',

				'![Gradebook](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_grades_en_2021.png)',

				'![Take Attendance](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_attendance_en_2021.png)',

				'![Discipline](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_discipline_en_2021.png)',

				'![Fees](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_student_billing_en_2021.png)',
			],
			'',
			$md
		);
	}

	$html = MarkDownToHTML( $md );

	$handle = PDFStart( array(
		'mode' => 3, // MODE_SAVE.
	) );

	echo $html;

	// Fix PDFStop() since 11.2 is not using $RosarioPath but base URL.
	PDFStopLocalFileAccess( $handle );

	$pdf_file = dirname( __FILE__, 2 ) . DIRECTORY_SEPARATOR . str_replace( '.md', '.pdf', $md_file );

	$tmp_pdf_file = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'RosarioSIS' . ++$i . '.pdf';

	if ( ! file_exists( $tmp_pdf_file ) )
	{
		return false;
	}

	$pdf = file_get_contents( $tmp_pdf_file );

	return file_put_contents( $pdf_file, $pdf );
}

/**
 * Get buffer and generate PDF
 * Renders HTML if not wkhtmltopdf
 *
 * @since 3.4 Handle HTML header & footer.
 * @since 4.3 CSS Add .wkhtmltopdf-header, .wkhtmltopdf-footer, .wkhtmltopdf-portrait & .wkhtmltopdf-landscape classes
 * @since 7.5 Use phpwkhtmltopdf class instead of Wkhtmltopdf (more reliable & faster)
 * @since 10.9 CSS Add modname class, ie .grades-reportcards-php for modname=Grades/ReportCards.php
 * @link https://github.com/mikehaertl/phpwkhtmltopdf
 *
 * @global string $wkhtmltopdfPath
 * @global string $wkhtmltopdfAssetsPath
 * @global string $RosarioPath
 *
 * @param  array $handle from PDFStart(), PDF options.
 *
 * @return string Full path to file if Save mode, else outputs HTML if not wkhtmltopdf or Embed / Download PDF
 */
function PDFStopLocalFileAccess( $handle )
{
	global $wkhtmltopdfPath,
		$wkhtmltopdfAssetsPath,
		$RosarioPath;

	static $file_number = 1;

	if ( ! $handle )
	{
		return '';
	}

	$handle['orientation'] = empty( $_SESSION['orientation'] ) ? $handle['orientation'] : $_SESSION['orientation'];

	unset( $_SESSION['orientation'] );

	// Get buffer.
	$html_content = ob_get_clean();

	$lang_2_chars = mb_substr( $_SESSION['locale'], 0, 2 );

	// Right to left direction.
	$RTL_languages = [ 'ar', 'he', 'dv', 'fa', 'ur', 'ps' ];

	$dir_RTL = in_array( $lang_2_chars, $RTL_languages ) ? ' dir="RTL"' : '';

	// Page width.
	// @see wkhtmltopdf.css.
	$orientation_class = 'wkhtmltopdf-portrait'; // 994px, originally 1024px.

	if ( $handle['orientation'] === 'landscape' )
	{
		$orientation_class = 'wkhtmltopdf-landscape'; // 1405px, originally 1448px.
	}

	$modname_class = '';

	if ( $_REQUEST['modname'] )
	{
		$modname_class = 'modname-' . mb_strtolower( preg_replace(
			'/([^\-a-z0-9]+)/i',
			'-',
			$_REQUEST['modname']
		) );
	}

	// Page title.
	$page_title = str_replace( _( 'Print' ) . ' ', '', ProgramTitle() );

	$_html = [];

	// Convert to HTML page with CSS.
	$_html['head'] = '<!doctype html>
		<html lang="' . $lang_2_chars . '" ' . $dir_RTL . '>
		<head>
			<meta charset="UTF-8">';

	if ( $handle['css'] )
	{
		$_html['head'] .= '<link rel="stylesheet" type="text/css" href="assets/themes/' . Preferences( 'THEME' ) . '/stylesheet_wkhtmltopdf.css">';
	}

	// Include Markdown to HTML.
	// @since 6.0 JS MarkDown use marked instead of showdown (15KB smaller).
	$_html['head'] .= '<script src="assets/js/marked/marked.min.js"></script>';

	// Include wkhtmltopdf Warehouse JS functions.
	$_html['head'] .= '<script src="assets/js/warehouse_wkhtmltopdf.js"></script>';

	// FJ bugfix wkhtmltopdf screen resolution on linux
	// see: https://code.google.com/p/wkhtmltopdf/issues/detail?id=118
	$_html['head'] .= '<title>' . $page_title . '</title>
		</head>
		<body>
			<div class="wkhtmltopdf-body-wrapper ' . $orientation_class . ' ' . $modname_class . '" id="pdf">';

	$_html['foot'] = '</div>
		</body>
		</html>';

	$html = $_html['head'] . $html_content . $_html['foot'];

	// Create PDF in the temporary files system directory.
	$path = sys_get_temp_dir();

	// File name.
	// Fix PHP8.2 utf8_decode() function deprecated
	// Decode UTF8 is useful for Windows only.
	$filename = iconv(
		'UTF-8',
		'ISO-8859-1',
		str_replace(
			[ _( 'Print' ) . ' ', ' ' ],
			[ '', '_' ],
			ProgramTitle()
		)
	) . ( $file_number++ );

	if ( empty( $wkhtmltopdfPath ) )
	{
		// If no wkhtmltopdf, render in HTML.
		if ( $handle['mode'] !== 3 ) // Display HTML.
		{
			echo $html;

			return '';
		}

		// Save.
		$base_url = sprintf(
			'%s://%s%s/',
			isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			dirname( $_SERVER['PHP_SELF'] )
		);

		// Set Absolute URLs to images, CSS...
		$html = str_replace( '"assets/', '"' . $base_url . 'assets/', $html );

		$html = str_replace( '"modules/', '"' . $base_url . 'modules/', $html );

		file_put_contents( $path . DIRECTORY_SEPARATOR . $filename . '.html', $html );

		return $path . DIRECTORY_SEPARATOR . $filename . '.html';
	}

	// Load phpwkhtmltopdf class.
	require_once 'classes/phpwkhtmltopdf/php-shellcommand/Command.php';
	require_once 'classes/phpwkhtmltopdf/php-tmpfile/File.php';
	require_once 'classes/phpwkhtmltopdf/Command.php';
	require_once 'classes/phpwkhtmltopdf/Pdf.php';

	// You can override the Path definition in the config.inc.php file.
	if ( ! isset( $wkhtmltopdfAssetsPath ) )
	{
		// Way wkhtmltopdf accesses the assets/ directory, empty string means no translation.
		$wkhtmltopdfAssetsPath = $RosarioPath . 'assets/';
	}

	if ( ! empty( $wkhtmltopdfAssetsPath ) )
	{
		// Fix wkhtmltopdf error on Windows: prepend file:///.
		$html = str_replace( '"assets/', '"file:///' . $wkhtmltopdfAssetsPath, $html );

		$_html['head'] = str_replace( '"assets/', '"file:///' . $wkhtmltopdfAssetsPath, $_html['head'] );
	}

	// Fix wkhtmltopdf error on Windows: prepend file:///.
	$html = str_replace( '"modules/', '"file:///' . $RosarioPath . 'modules/', $html );

	// Fix wkhtmltopdf not rendering URL in CSS
	$html = str_replace( 'url(modules/', 'url(file:///' . $RosarioPath . 'modules/', $html );

	// Set wkhtmltopdf options.
	$pdf_options = [
		'title' => $page_title,
		// Fix System error "blocked access to local file" with wkhtmltopdf 0.12.6.
		'enable-local-file-access',
	];

	if ( Preferences( 'PAGE_SIZE' ) != 'A4' )
	{
		$pdf_options['page-size'] = Preferences( 'PAGE_SIZE' );
	}

	if ( ! empty( $handle['orientation'] )
		&& $handle['orientation'] === 'landscape' )
	{
		$pdf_options['orientation'] = 'Landscape';
	}

	if ( ! empty( $handle['margins'] )
		&& is_array( $handle['margins'] ) )
	{
		foreach ( $handle['margins'] as $position => $margin )
		{
			if ( is_null( $margin ) )
			{
				continue;
			}

			$pdf_options['margin-' . $position] = $margin;
		}
	}

	if ( $handle['header_html'] )
	{
		$header_html = $handle['header_html'];

		if ( mb_stripos( $header_html, '<html' ) === false )
		{
			// Build full HMTL page.
			// Fix HTML header not showing, remove CSS width & height 100%.
			$header_html = str_replace(
				'<html',
				'<html class="wkhtmltopdf-header"',
				$_html['head']
			) .
			$header_html . $_html['foot'];
		}

		$pdf_options['header-html'] = $header_html;
	}

	if ( $handle['footer_html'] )
	{
		$footer_html = $handle['footer_html'];

		if ( mb_stripos( $footer_html, '<html' ) === false )
		{
			// Build full HMTL page.
			// Fix HTML footer, remove CSS width & height 100%.
			$footer_html = str_replace(
				'<html',
				'<html class="wkhtmltopdf-footer"',
				$_html['head']
			) .
			$footer_html . $_html['foot'];
		}

		$pdf_options['footer-html'] = $footer_html;
	}

	$pdf = new mikehaertl\wkhtmlto\Pdf( $pdf_options );

	if ( ! function_exists( 'proc_open' ) )
	{
		// @since 8.8 Fix proc_open() PHP function not allowed.
		// Use `exec()` instead of `proc_open()`.
		$pdf->commandOptions['useExec'] = true;

		if ( ! function_exists( 'exec' ) )
		{
			// exec() PHP function not allowed either, error.
			echo ErrorMessage( [ 'proc_open and exec PHP functions are disabled. Cannot call wkhtmltopdf and generate PDF. Contact your server administrator for more information.' ] );

			return '';
		}
	}

	$pdf->binary = $wkhtmltopdfPath;

	$pdf->addPage( $html );

	if ( $handle['mode'] === 3 ) // Save.
	{
		$full_path = $path . DIRECTORY_SEPARATOR . $filename . '.pdf';

		// Save the PDF.
		if ( ! $pdf->saveAs( $full_path ) )
		{
			echo ErrorMessage( [ $pdf->getError() ] );
		}

		return $full_path;
	}

	// Send to client as file download.
	if ( ! $pdf->send( $filename . '.pdf', (bool) $handle['mode'] ) ) // Embed or Download.
	{
		echo ErrorMessage( [ $pdf->getError() ] );
	}

	return '';
}
