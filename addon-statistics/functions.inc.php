<?php
/**
 * Add-on Statistics functions
 */

/*
CREATE TABLE addon_installations (
    ip character varying(50) NOT NULL,
    user_agent text,
    created_at timestamp(0) without time zone NOT NULL,
    addon_dir character varying(70),
    type character varying(15),
    lang character varying(10),
    rosario_version character varying(20)
);

CREATE VIEW addon_installations_count AS
	SELECT addon_dir, COUNT(*) AS count
	FROM addon_installations
	GROUP BY addon_dir
	ORDER BY count DESC;
 */

function AddonInstallationSave( $poll_data )
{
	$spam_sql = "SELECT COUNT(1) FROM addon_installations
		WHERE IP='" . DBEscapeString( $poll_data['IP'] ) . "'
		AND CREATED_AT>=(CURRENT_TIMESTAMP - INTERVAL '1 day')";

	$spam_count = (int) DBGetOne( $spam_sql );

	if ( $spam_count > 100 ) // We have 52 official add-ons as of 2023-02-21.
	{
		error_log( 'HACKING ATTEMPT: ' . $spam_count . ' add-on installations in a day from IP ' . $poll_data['IP'] );

		return false;
	}

	$already_installed_today_sql = "SELECT 1 FROM addon_installations
		WHERE IP='" . DBEscapeString( $poll_data['IP'] ) . "'
		AND USER_AGENT='" . DBEscapeString( $poll_data['USER_AGENT'] ) . "'
		AND ADDON_DIR='" . DBEscapeString( $poll_data['ADDON_DIR'] ) . "'
		AND CREATED_AT>=(CURRENT_TIMESTAMP - INTERVAL '1 day')";

	if ( DBGetOne( $already_installed_today_sql ) )
	{
		return false;
	}

	$insert_sql = "INSERT INTO addon_installations (IP,USER_AGENT,CREATED_AT,ADDON_DIR,TYPE,LANG,ROSARIO_VERSION)
	VALUES('" . DBEscapeString( $poll_data['IP'] ) . "','" . DBEscapeString( $poll_data['USER_AGENT'] ) . "','" .
	DBEscapeString( $poll_data['CREATED_AT'] ) . "','" . DBEscapeString( $poll_data['ADDON_DIR'] ) . "','" .
	DBEscapeString( $poll_data['TYPE'] ) . "','" . DBEscapeString( $poll_data['LANG'] ) . "','" .
	DBEscapeString( $poll_data['ROSARIO_VERSION'] ) . "')";

	return DBQuery( $insert_sql );
}


/**
 * Establish DB connection
 *
 * @global $DatabaseServer   Database server hostname
 * @global $DatabaseUsername Database username
 * @global $DatabasePassword Database password
 * @global $DatabaseName     Database name
 * @global $DatabasePort     Database port
 * @see config.inc.php file for globals definitions
 *
 * @return PostgreSQL connection resource
 */
function db_start()
{
	global $DatabaseServer,
		$DatabaseUsername,
		$DatabasePassword,
		$DatabaseName,
		$DatabasePort;

	/**
	 * Fix pg_connect(): Unable to connect to PostgreSQL server:
	 * could not connect to server:
	 * No such file or directory Is the server running locally
	 * and accepting connections on Unix domain socket "/tmp/.s.PGSQL.5432"
	 *
	 * Always set host, force TCP.
	 *
	 * @since 3.5.2
	 */
	$connectstring = 'host=' . $DatabaseServer . ' ';

	if ( $DatabasePort !== '5432' )
	{
		$connectstring .= 'port=' . $DatabasePort . ' ';
	}

	$connectstring .= 'dbname=' . $DatabaseName . ' user=' . $DatabaseUsername;

	if ( $DatabasePassword !== '' )
	{
		$connectstring .= ' password=' . $DatabasePassword;
	}

	$connection = pg_connect( $connectstring );

	// Error code for both.

	if ( $connection === false )
	{
		// TRANSLATION: do NOT translate these since error messages need to stay in English for technical support.
		error_log(
			sprintf( "Could not Connect to Database Server '%s'.", $DatabaseServer ) . ' ' . pg_last_error()
		);
	}

	return $connection;
}


/**
 * This function connects, and does the passed query, then returns a result resource
 * Not receiving the return == unusable search.
 *
 * @example $processable_results = DBQuery( "SELECT * FROM students" );
 *
 * @since 3.7 INSERT INTO case to Replace empty strings ('') with NULL values.
 * @since 4.3 Performance: static DB $connection.
 * @since 4.3 Do DBQuery after action hook.
 *
 * @param  string   $sql       SQL statement.
 * @return resource PostgreSQL result resource
 */
function DBQuery( $sql )
{
	static $connection;

	if ( ! isset( $connection ) )
	{
		$connection = db_start();
	}

	// Replace empty strings ('') with NULL values.

	if ( stripos( $sql, 'INSERT INTO ' ) !== false )
	{
		// Check for ( or , character before empty string ''.
		$sql = preg_replace( "/([,\(])[\r\n\t ]*''(?!')/", '\\1NULL', $sql );
	}

	// Check for <> or = character before empty string ''.
	$sql = preg_replace( "/(<>|=)[\r\n\t ]*''(?!')/", '\\1NULL', $sql );

	/**
	 * IS NOT NULL cases
	 *
	 * Replace <>NULL & !=NULL with IS NOT NULL
	 *
	 * @link http://www.postgresql.org/docs/current/static/functions-comparison.html
	 */
	$sql = str_replace(
		array( '<>NULL', '!=NULL' ),
		array( ' IS NOT NULL', ' IS NOT NULL' ),
		$sql
	);

	$result = @pg_exec( $connection, $sql );

	if ( $result === false )
	{
		// TRANSLATION: do NOT translate these since error messages need to stay in English for technical support.
		error_log(
			'DB Execute Failed: ' . pg_last_error( $connection )
		);
	}

	return $result;
}


/**
 * DB Get One
 * Get one (first) column (& row) value from database.
 *
 * @since 4.5
 *
 * @example $school_year = DBGetOne( "SELECT SYEAR FROM TABLE WHERE SYEAR='2019'" );
 *
 * @param string $sql_select SQL SELECT query.
 *
 * @return string False or DB value.
 */
function DBGetOne( $sql_select )
{
	$QI = DBQuery( $sql_select );

	$RET = (array) db_fetch_row( $QI );

	return reset( $RET );
}


/**
 * DB Get
 * Get one (first) row from database.
 *
 * @since 4.5
 *
 * @example $school_year = DBGet( "SELECT SYEAR FROM TABLE WHERE SYEAR='2019'" );
 *
 * @param string $sql_select SQL SELECT query.
 *
 * @return string False or DB value.
 */
function DBGet( $sql_select )
{
	$QI = DBQuery( $sql_select );

	$RET = array();

	while ( $row = db_fetch_row( $QI ) )
	{
		$RET[] = $row;
	}

	return $RET;
}


/**
 * Return next row
 *
 * @param  resource PostgreSQL result resource $result Result.
 * @return array    	Next row in result set.
 */
function db_fetch_row( $result )
{
	$return = @pg_fetch_array( $result, null, PGSQL_ASSOC );

	return is_array( $return ) ? @array_change_key_case( $return, CASE_UPPER ) : $return;
}


/**
 * Escapes single quotes by using two for every one.
 *
 * @example $safe_string = DBEscapeString( $string );
 *
 * @param  string $input  Input string.
 * @return string escaped string
 */
function DBEscapeString( $input )
{
	// return str_replace("'","''",$input);
	return pg_escape_string( $input );
}

/**
 * Escapes identifiers (table, column) using double quotes.
 * Security function for
 * when you HAVE to use a variable as an identifier.
 *
 * @example $safe_sql = "SELECT COLUMN FROM " . DBEscapeIdentifier( $table ) . " WHERE " . DBEscapeIdentifier( $column ) . "='Y'";
 * @uses pg_escape_identifier(), requires PHP 5.4.4+
 * @since 3.0
 *
 * @param  string $identifier SQL identifier (table, column).
 * @return string Escaped identifier.
 */
function DBEscapeIdentifier( $identifier )
{
	$identifier = mb_strtolower( $identifier );

	if ( ! function_exists( 'pg_escape_identifier' ) )
	{
		return '"' . $identifier . '"';
	}

	return pg_escape_identifier( $identifier );
}
