<?php
/**
 * Add-on Installation Statistics for RosarioSIS
 */

require_once 'config.inc.php';
require_once 'functions.inc.php';

if ( empty( $_POST ) )
{
	if ( ! empty( $_SERVER['HTTP_USER_AGENT'] ) )
	{
		header( 'Location: index.php' );
	}

	// Ping from instance.
	die( '1' );
}
else
{
	// Fix XHR blocked CORS Missing Allow Origin
	header( 'Access-Control-Allow-Origin: *' );

	if ( ( empty( $_POST['addon_dir'] )
			|| strpos( $_POST['addon_dir'], ' ' ) !== false
			|| strlen( $_POST['addon_dir'] ) > 70 )
		|| ( empty( $_POST['type'] )
			|| ! in_array( $_POST['type'], [ 'module', 'plugin' ] ) ) )
	{
		// Invalid addon dir or type.
		die( 'Invalid addon dir or type.' );
	}

	if ( empty( $_POST['lang'] )
			|| strlen( $_POST['lang'] ) !== 5 )
	{
		// Invalid lang.
		die( 'Invalid lang.' );
	}

	$ip = ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )
		// Filter IP, HTTP_* headers can be forged.
		&& filter_var( $_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP ) ?
		$_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] );

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$installation_data = [];

	$installation_data['IP'] = $ip;

	$installation_data['USER_AGENT'] = $user_agent;

	$installation_data['CREATED_AT'] = date( 'Y-m-d H:i:s' );

	$installation_data['ADDON_DIR'] = $_POST['addon_dir'];

	$installation_data['TYPE'] = $_POST['type'];

	$installation_data['LANG'] = $_POST['lang'];

	// @since RosarioSIS 12.2
	$installation_data['ROSARIO_VERSION'] = isset( $_POST['rosario_version'] ) ? $_POST['rosario_version'] : '';

	// Save info to DB.
	$saved = AddonInstallationSave( $installation_data );

	die( 'Addon installation' . ( $saved ? ' ' : ' not ' ) . 'saved!' );
}
