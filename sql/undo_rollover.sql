--
-- PostgreSQL script to undo Rollover
--
-- This will delete school data in the next school year
-- Set the `next_syear` and `user_school` variables accordingly
-- @link https://stackoverflow.com/questions/766657/how-do-you-use-variables-in-a-simple-postgresql-script
--

DO $$
DECLARE
	-- `next_syear` should be the school year you want to delete
	next_syear integer := 2025;
	-- `user_school` should be the school ID
	user_school integer := 1;
BEGIN
	DELETE FROM discipline_field_usage
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM eligibility_activities
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM program_config WHERE SYEAR=next_syear;

	DELETE FROM report_card_comment_categories
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM report_card_comments
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM report_card_grade_scales
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM report_card_grades
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM student_enrollment
	WHERE SYEAR=next_syear
	AND LAST_SCHOOL=user_school;

	DELETE FROM student_enrollment_codes WHERE SYEAR=next_syear;

	DELETE FROM gradebook_assignments
	WHERE COURSE_ID IN(SELECT COURSE_ID FROM courses
		WHERE SYEAR=next_syear
		AND SCHOOL_ID=user_school)
	OR COURSE_PERIOD_ID IN(SELECT COURSE_PERIOD_ID FROM course_periods
		WHERE SYEAR=next_syear
		AND SCHOOL_ID=user_school);

	DELETE FROM gradebook_assignment_types
	WHERE COURSE_ID IN(SELECT COURSE_ID FROM courses
		WHERE SYEAR=next_syear
		AND SCHOOL_ID=user_school);

	DELETE FROM schedule_requests
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM course_period_school_periods
	WHERE COURSE_PERIOD_ID IN (SELECT COURSE_PERIOD_ID
	FROM course_periods
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school);

	DELETE FROM course_periods
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM courses
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM course_subjects
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM attendance_codes
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM attendance_code_categories
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM attendance_calendars
	WHERE SCHOOL_ID=user_school
	AND SYEAR=next_syear;

	DELETE FROM school_marking_periods
	WHERE SYEAR=next_syear
	AND SCHOOL_ID=user_school;

	DELETE FROM school_periods
	WHERE SCHOOL_ID=user_school
	AND SYEAR=next_syear;

	DELETE FROM food_service_staff_accounts
	WHERE exists(SELECT 1 FROM staff
		WHERE STAFF_ID=food_service_staff_accounts.STAFF_ID
		AND SYEAR=next_syear);

	DELETE FROM students_join_users
	WHERE STAFF_ID IN (SELECT STAFF_ID FROM staff WHERE SYEAR=next_syear);

	DELETE FROM staff_exceptions
	WHERE USER_ID IN (SELECT STAFF_ID FROM staff WHERE SYEAR=next_syear);

	DELETE FROM program_user_config
	WHERE USER_ID IN (SELECT STAFF_ID FROM staff WHERE SYEAR=next_syear);

	DELETE FROM staff WHERE SYEAR=next_syear;

	DELETE FROM schools WHERE SYEAR=next_syear;
END $$;
