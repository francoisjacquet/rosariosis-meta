--
-- PostgreSQL data update
--
-- Add demo data.
--
-- Note: Uncheck "Paginate results" when importing with phpPgAdmin
--

--
-- Login 'administrator'
--

UPDATE staff SET USERNAME='administrator',PASSWORD='$6$5d278a1bb2337a45$KkoYMHuGsCAuPGpMKRQsWzftYTwZA6JYjyW3UahzpoXmmGm9RZNSkva74cf01nW9Fdt2RQZuwwXOHenPrr2gB/'
WHERE STAFF_ID='1';


--
--  Login = 'Yes'
--

UPDATE config SET CONFIG_VALUE='Yes' WHERE TITLE='LOGIN';


--
-- Data for Name: attendance_calendar; Type: TABLE DATA; Schema: public; Owner: rosariosis_demo
-- https://stackoverflow.com/questions/9743810/calculate-closest-working-day-in-postgres
-- https://stackoverflow.com/questions/14113469/generating-time-series-between-two-dates-in-postgresql
--

INSERT INTO attendance_calendar (syear, school_id, school_date, minutes, calendar_id)
SELECT SYEAR, 1, working_day_i::date, 999, 1
FROM SCHOOLS,
	GENERATE_SERIES((SELECT START_DATE FROM SCHOOL_MARKING_PERIODS WHERE MP='FY' LIMIT 1)::timestamp,
	(SELECT END_DATE FROM SCHOOL_MARKING_PERIODS WHERE MP='FY' LIMIT 1)::timestamp, '1 day') working_day_i
WHERE (EXTRACT(ISODOW FROM working_day_i::date)::integer) % 7 != 6
AND (EXTRACT(ISODOW FROM working_day_i::date)::integer) % 7 != 0
AND ID=1;


--
-- Data for Name: schools; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

UPDATE schools
SET www_address='www.rosariosis.com/fr'
WHERE id=1;


--
-- Allow Teachers to edit grades after grade posting period
--

UPDATE PROGRAM_CONFIG SET VALUE='Y' WHERE TITLE='GRADES_TEACHER_ALLOW_EDIT';


--
-- Enable Anonymous Grade Statistics for Parents and Students
--

UPDATE PROGRAM_CONFIG SET VALUE='Y' WHERE TITLE='GRADES_DO_STATS_STUDENTS_PARENTS';


--
-- Portal Note: Tip: user login
--

INSERT INTO "portal_notes" ("id", "school_id", "syear", "title", "content", "sort_order", "published_user", "published_date", "start_date", "end_date", "published_profiles", "file_attached") VALUES
(NEXTVAL('portal_notes_id_seq'),	1,	(SELECT syear FROM schools WHERE id=1),	'Tip: login utilisateur',	'Vous pouvez entrer comme
- Enseignant: nom d''utilisateur et mot de passe `teacher`
- Élève: nom d''utilisateur et mot de passe `student`
- Parent: nom d''utilisateur et mot de passe `parent`',	NULL,	1,	CURRENT_TIMESTAMP,	NULL,	NULL,	',1,',	NULL);


--
-- Calendar Event: Family Night: Tech Talk
--

INSERT INTO "calendar_events" ("id", "syear", "school_id", "school_date", "title", "description") VALUES
(NEXTVAL('calendar_events_id_seq'),	(SELECT syear FROM schools WHERE id=1),	1,	(CURRENT_DATE + 1),	'Soirée Familles: Discussions Technologie',	'Discussions Technologie : Internet et la sécurité pour nos enfants.
- Quiz "Tout savoir sur ces applis".
- Bingo "Les cracks du cyberespace".
- Partagez les expériences de vos enfants sur le "mur des curiosités".

Streaming live sur notre site web.

Babysitting gratuit.');


--
-- Student2
--

INSERT INTO "students" ("student_id", "last_name", "first_name", "middle_name", "name_suffix", "username", "password", "last_login", "failed_login", "custom_200000000", "custom_200000001", "custom_200000002", "custom_200000003", "custom_200000004", "custom_200000005", "custom_200000006", "custom_200000007", "custom_200000008", "custom_200000009", "custom_200000010", "custom_200000011") VALUES
(NEXTVAL('students_student_id_seq'),	'Élève2',	'Student2',	'S',	NULL,	'student2',	'$6$9a338189e938052d$xWKLbZ4szgrYA9/X66kz319j6QwhyQMtwG/wwqXbmDk0wl2WPjGBo0Q5EIOwPi5lPy1KaRYCNrh7wbXvaJXQ9/',	NULL,	NULL,	'Féminin',	'Hispanique',	NULL,	NULL,	'2007-12-10',	'Français',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);


--
-- Student2 enrollment
--

INSERT INTO "student_enrollment" ("id", "syear", "school_id", "student_id", "grade_id", "start_date", "end_date", "enrollment_code", "drop_code", "next_school", "calendar_id", "last_school") VALUES
(NEXTVAL('student_enrollment_id_seq'),	(SELECT syear FROM schools WHERE id=1),	1,	2,	7,	CURRENT_DATE,	NULL,	6,NULL,	1,	1,	NULL);


--
-- Student2 FS account
--

INSERT INTO "food_service_accounts" VALUES (2, 0.00, NULL);

INSERT INTO "food_service_student_accounts" ("student_id", "account_id", "discount", "status", "barcode") VALUES
(2,	2,	NULL,	NULL,	'1000002');



--
-- Parent2
--

INSERT INTO "staff" ("syear", "staff_id", "current_school_id", "title", "first_name", "last_name", "middle_name", "name_suffix", "username", "password", "custom_200000001", "email", "profile", "homeroom", "schools", "last_login", "failed_login", "profile_id", "rollover_id") VALUES
((SELECT syear FROM schools WHERE id=1),	NEXTVAL('staff_staff_id_seq'),	NULL,	NULL,	'Parent2',	'Parent2',	'P',	NULL,	'parent2',	'$6$248b28dc423dc7aa$WZ9jBbpFWLWngrMk4mw3yXaLplJBv3Cz0Eqoa9xisSA7GSLvRRKicFyhD6pCYyuSv3tdtSOYpDfzEZkl0Rg4Q1',	'0123456789',	NULL,	'parent',	NULL,	NULL,	NULL,	NULL,	3,	NULL);


--
-- Student2 join Parent2
--

INSERT INTO "students_join_users" ("student_id", "staff_id") VALUES
(2,	4);


--
-- Student 1 & 2 Addresses
--

INSERT INTO "address" ("address_id", "house_no", "direction", "street", "apt", "zipcode", "city", "state", "mail_street", "mail_city", "mail_state", "mail_zipcode", "address", "mail_address", "phone") VALUES
(NEXTVAL('address_address_id_seq'),	NULL,	NULL,	NULL,	NULL,	'75001',	'Paris',	NULL,	NULL,	NULL,	NULL,	NULL,	'12 avenue Foch',	NULL,	'0123456789'),
(NEXTVAL('address_address_id_seq'),	NULL,	NULL,	NULL,	NULL,	'75001',	'Paris',	NULL,	NULL,	NULL,	NULL,	NULL,	'24 rue Monplaisir',	NULL,	'0987654321');


--
-- Student 1 & 2 join addresses
--

INSERT INTO "students_join_address" ("id", "student_id", "address_id", "contact_seq", "gets_mail", "primary_residence", "legal_residence", "am_bus", "pm_bus", "mailing", "residence", "bus", "bus_pickup", "bus_dropoff") VALUES
(NEXTVAL('students_join_address_id_seq'),	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'Y',	'Y',	NULL,	'Y',	'Y'),
(NEXTVAL('students_join_address_id_seq'),	2,	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'Y',	'Y',	NULL,	'Y',	'Y');


--
-- Teacher2
--

INSERT INTO "staff" ("syear", "staff_id", "current_school_id", "title", "first_name", "last_name", "middle_name", "name_suffix", "username", "password", "custom_200000001", "email", "profile", "homeroom", "schools", "last_login", "failed_login", "profile_id", "rollover_id") VALUES
((SELECT syear FROM schools WHERE id=1),	NEXTVAL('staff_staff_id_seq'),	NULL,	NULL,	'Teacher2',	'Enseignant2',	'T',	NULL,	'teacher2',	'$6$a1c6140a78a32c73$7af.or9exWfWcVcp2RWtO8DF2f07JIo26aiTw.uadrfbozkfrYHOJp03MyPa0y6gZo4WZgngVTEkgCpYLh16T1',	NULL,	NULL,	'teacher',	NULL,	NULL,	NULL,	NULL,	2,	NULL);


--
-- Subjects Mathématiques + Espagnol
--

INSERT INTO "course_subjects" ("syear", "school_id", "subject_id", "title", "short_name", "sort_order", "rollover_id") VALUES
((SELECT syear FROM schools WHERE id=1),	1,	NEXTVAL('course_subjects_subject_id_seq'),	'Mathématiques',	NULL,	1,	NULL),
((SELECT syear FROM schools WHERE id=1),	1,	NEXTVAL('course_subjects_subject_id_seq'),	'Espagnol',	NULL,	2,NULL);


--
-- Courses Mathématiques 6 + Espagnol 6
--

INSERT INTO "courses" ("syear", "course_id", "subject_id", "school_id", "grade_level", "title", "short_name", "rollover_id", "credit_hours", "description") VALUES
((SELECT syear FROM schools WHERE id=1),	NEXTVAL('courses_course_id_seq'),	1,	1,	NULL,	'Mathématiques 6','Math6',	NULL,	NULL,	'<p>Cours de Mathématiques pour les élèves de 6ème.</p>'),
((SELECT syear FROM schools WHERE id=1),	NEXTVAL('courses_course_id_seq'),	2,	1,	NULL,	'Espagnol 6',	'Espa6',	NULL,	NULL,	'<p>Cours d''Espagnol pour les élèves de 6ème.</p>');


--
-- Course Periods Half Day AM - Math6A - Teach T Teacher + Half Day PM - Spa6A - Teacher2 T Teacher2
--

INSERT INTO "course_periods" ("syear", "school_id", "course_period_id", "course_id", "title", "short_name", "mp", "marking_period_id", "teacher_id", "room", "total_seats", "filled_seats", "does_attendance", "does_honor_roll", "does_class_rank", "gender_restriction", "house_restriction", "availability", "parent_id", "calendar_id", "half_day", "does_breakoff", "rollover_id", "grade_scale_id", "credits") VALUES
((SELECT syear FROM schools WHERE id=1),	1,	NEXTVAL('course_periods_course_period_id_seq'),	1,	'Matin - Math6A - Teach T Enseignant',	'Math6A',	'FY',	1,	2,	'101',	20,	0,	',0,',	'Y',	'Y',	'N',	NULL,	NULL,	1,	1,	NULL,	NULL,	NULL,	1,	5),
((SELECT syear FROM schools WHERE id=1),	1,	NEXTVAL('course_periods_course_period_id_seq'),	2,	'Après-midi - Espa6 - Teacher2 T Enseignant2',	'Espa6A',	'FY',	1,	5,	'202',	20,	0,	',0,',	'Y',	'Y',	'N',	NULL,	NULL,	2,	1,	NULL,	NULL,	NULL,	1,	3);


--
-- CPSP Mathématiques 6 + Espagnol 6
--

INSERT INTO "course_period_school_periods" ("course_period_school_periods_id", "course_period_id", "period_id", "days") VALUES
(NEXTVAL('course_period_school_periods_course_period_school_periods_i_seq'),	1,	2,	'MTWHF'),
(NEXTVAL('course_period_school_periods_course_period_school_periods_i_seq'),	2,	3,	'MTWHF');


--
-- Schedule Students to Mathématiques 6 + Espagnol 6
--

INSERT INTO "schedule" ("syear", "school_id", "student_id", "start_date", "end_date", "modified_date", "modified_by", "course_id", "course_period_id", "mp", "marking_period_id", "scheduler_lock", "id") VALUES
((SELECT syear FROM schools WHERE id=1),	1,	1,	CURRENT_DATE,	NULL,	NULL,	NULL,	1,	1,	'FY',	1,NULL,	NULL),
((SELECT syear FROM schools WHERE id=1),	1,	1,	CURRENT_DATE,	NULL,	NULL,	NULL,	2,	2,	'FY',	1,NULL,	NULL),
((SELECT syear FROM schools WHERE id=1),	1,	2,	CURRENT_DATE,	NULL,	NULL,	NULL,	1,	1,	'FY',	1,NULL,	NULL),
((SELECT syear FROM schools WHERE id=1),	1,	2,	CURRENT_DATE,	NULL,	NULL,	NULL,	2,	2,	'FY',	1,NULL,	NULL);


--
-- Assignment types: Devoirs à la maison
--

INSERT INTO "gradebook_assignment_types" ("assignment_type_id", "staff_id", "course_id", "title", "final_grade_percent", "sort_order", "color", "created_mp") VALUES
(NEXTVAL('gradebook_assignment_types_assignment_type_id_seq'),	2,	1,	'Devoirs à la maison',	NULL,	NULL,	'#002fbd',5),
(NEXTVAL('gradebook_assignment_types_assignment_type_id_seq'),	5,	2,	'Devoirs à la maison',	NULL,	NULL,	'#103cc7',4);


--
-- Assignments (Math6A + Spa6A)
--

INSERT INTO "gradebook_assignments" ("assignment_id", "staff_id", "marking_period_id", "course_period_id", "course_id", "assignment_type_id", "title", "assigned_date", "due_date", "points", "description", "file", "default_points", "submission") VALUES
(NEXTVAL('gradebook_assignments_assignment_id_seq'),	2,	(SELECT MARKING_PERIOD_ID FROM SCHOOL_MARKING_PERIODS WHERE MP='QTR' AND END_DATE>=CURRENT_DATE LIMIT 1),	1,	NULL,	1,	'Ajouter et soustraire',	CURRENT_DATE,	(CURRENT_DATE + 2),10,	'<p>Additions et soustractions.</p>
<p>&nbsp;</p>
<ul>
<li>10 + 20 =</li>
<li>5 - 15 =</li>
<li>36 + 9 =</li>
<li>51 - 12 =</li>
</ul>
<p>&nbsp;</p>',	NULL,	NULL,	'Y'),
(NEXTVAL('gradebook_assignments_assignment_id_seq'),	5,	(SELECT MARKING_PERIOD_ID FROM SCHOOL_MARKING_PERIODS WHERE MP='QTR' AND END_DATE>=CURRENT_DATE LIMIT 1),	2,	NULL,	2,	'Como se llama?',	CURRENT_DATE,	(CURRENT_DATE + 2),	10,'<p>Como se llama...</p>
<ul>
<li>Lo verde que crece en la tierra?</li>
<li>El bot&oacute;n que sirve para prender la luz?</li>
<li>La plataforma que est&aacute; usando?</li>
</ul>',	NULL,	NULL,	'Y');


--
-- Discipline Referral Student + Teacher
--

INSERT INTO "discipline_referrals" ("id", "syear", "student_id", "school_id", "staff_id", "entry_date", "referral_date", "category_1", "category_2", "category_3", "category_4", "category_5", "category_6") VALUES
(NEXTVAL('discipline_referrals_id_seq'),	(SELECT syear FROM schools WHERE id=1),	1,	1,	2,	CURRENT_DATE,	CURRENT_DATE,	'||Injures, vulgarité, language offensif||Parle sans avoir la Parole||',	'20 Minutes',	'Y',	NULL,	NULL,	NULL);


--
-- Salary Teacher
--

INSERT INTO "accounting_salaries" ("staff_id", "assigned_date", "due_date", "comments", "id", "title", "amount", "school_id", "syear") VALUES
(2,	CURRENT_DATE,	NULL,	NULL,	NEXTVAL('accounting_salaries_id_seq'),	'Salaire',	2000,	1,	(SELECT syear FROM schools WHERE id=1));


--
-- Billing Fee Student
--

INSERT INTO "billing_fees" ("student_id", "assigned_date", "due_date", "comments", "id", "title", "amount", "school_id", "syear", "waived_fee_id") VALUES
(1,	CURRENT_DATE,	NULL,	NULL,	NEXTVAL('billing_fees_id_seq'),	'Frais de Scolarité mensuel',	200,	1,	(SELECT syear FROM schools WHERE id=1),	NULL);


--
-- Message from Teacher to Admin
--

INSERT INTO "messages" ("message_id", "syear", "school_id", "from", "recipients", "subject", "datetime", "data") VALUES
(NEXTVAL('messages_message_id_seq'),	(SELECT syear FROM schools WHERE id=1),	1,	'a:3:{s:7:"user_id";s:1:"2";s:3:"key";s:8:"staff_id";s:4:"name";s:18:"Teach T Enseignant";}',	'Admin Administrateur',	'Avez-vous reçu mon message?',	(CURRENT_DATE -1 || ' 16:58:26')::timestamp,	'a:1:{s:7:"message";s:74:"<p>Bonjour Admin,</p>
<p>&nbsp;</p>
<p>Avez-vous pu lire mon message ?</p>";}');

INSERT INTO "messagexuser" ("user_id", "key", "message_id", "status") VALUES
(1,	'staff_id',	1,	'unread'),
(2,	'staff_id',	1,	'sent');
