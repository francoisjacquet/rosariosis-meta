#!/bin/bash

# https://docs.gitlab.com/ee/ci/examples/php.html

# We need to install dependencies only for Docker.
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer.
# Install pgsql.
# https://github.com/docker-library/php/issues/221
# Install gd.
# https://stackoverflow.com/questions/39657058/installing-gd-in-docker#39658592
apt-get update -yqq
apt-get install git libpq-dev libpng-dev -yqq

# Install phpunit.
#curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
#chmod +x /usr/local/bin/phpunit

# Install lint.
cp qa/lint /usr/local/bin/lint
chmod +x /usr/local/bin/lint

# Install phpcompatinfo.
cp qa/phpcompatinfo.phar /usr/local/bin/phpcompatinfo
chmod +x /usr/local/bin/phpcompatinfo

# Install phpmd Mess Detector.
cp qa/phpmd.phar /usr/local/bin/phpmd
chmod +x /usr/local/bin/phpmd

# Install phpcpd Copy Paste Detector.
cp qa/phpcpd.phar /usr/local/bin/phpcpd
chmod +x /usr/local/bin/phpcpd


# Install PHP extensions.
docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
docker-php-ext-install pgsql gettext gd

# Clone rosariosis.
git clone https://gitlab.com/francoisjacquet/rosariosis.git
