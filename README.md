# RosarioSIS Meta

Tools for RosarioSIS:

- Build
- Debug
- Tests
- PHP Quality Assurance (QA)
- Refactoring
- Profiler
- Security
- log analysis
- Demo data
- SQL scripts

Place the tools in the `meta/` folder inside RosarioSIS.

Reports page: https://francoisjacquet.gitlab.io/rosariosis-meta/

## Build

### version-upgrade.sh

Interactive script (bash) to replace all occurrences of current version to the new version.

Example: `3.3.2` to `3.4`

Usage:
```bash
chmod +x version-upgrade.sh
./version-upgrade.sh
```


## Debug

### Kint
https://github.com/kint-php/kint/

```bash
cd debug
wget https://raw.githubusercontent.com/kint-php/kint/master/build/kint.phar
```

Instead of `var_dump` use:
```php
d( $my_var );
```

### PHP Debug Bar
http://phpdebugbar.com/

```bash
cd debug/php-debugbar
php composer.phar install --no-dev
```
Do not use, does not integrate well with RosarioSIS...


## Tests

Run all tests:
```bash
./tests-run.sh
```

TODO
- PHPUnit tests
https://github.com/sebastianbergmann/phpunit
https://github.com/fzaninotto/Faker
https://github.com/mockery/mockery

### Codeception
https://codeception.com/quickstart
https://codeception.com/docs/07-AdvancedUsage

```bash
wget https://codeception.com/codecept.phar
php codecept.phar bootstrap
```

https://codeception.com/docs/modules/WebDriver#chromedriver
Follow instructions or for Chromium:
```bash
sudo apt install chromium-driver
```
Put RosarioSIS URL into: `tests/acceptance.suite.yml`.

Create acceptance tests for Index:
```bash
php codecept.phar generate:cest acceptance Index
```

Run individual test:
```bash
./test-run.sh tests/acceptance/modules/Student_Billing/StudentFeesCest.php
```

Run test group:
```bash
./test-group-run.sh course
```


## PHP Quality Assurance (QA)

Run all QA tools at once:
```bash
sudo chmod +x qa-run.sh
./qa-run.sh
```

### PHP 5.6
https://stackoverflow.com/questions/46378017/install-php5-6-in-debian-9
Install with Ondřej Surý repository:
```bash
sudo apt install php5.6
```

### PHP lint
https://github.com/dprevite/lint

```bash
cd qa
wget https://raw.githubusercontent.com/dprevite/lint/master/scripts/lint
sudo chmod +x lint
sudo cp lint /usr/local/bin/lint
lint --exclude=node_modules,classes,PHPExcel,meta,vendor ../.. > ../public/lint.txt
php5.6 /usr/local/bin/lint --exclude=node_modules,classes,PHPExcel,meta,vendor ../.. > ../public/lint_php5.6.txt
```

WAPPstack PHP (Windows):
Add `php.exe` folder to Path in _Control Panel > System and Security > System > Advanced system settings > Environment Variables_.
```bash
wappstack-lint.bat
```


### SQLint - a simple SQL linter
https://github.com/purcell/sqlint

```bash
cd qa
sudo apt install ruby`ruby -e 'puts RUBY_VERSION[/\d+\.\d+/]'`-dev
sudo gem install sqlint
find ../.. -maxdepth 3 -type f -name "*.sql" -exec cat {} \; > all_sql
sqlint all_sql > ../public/sqlint.txt
rm all_sql
```

### sql-lint
https://github.com/joereynolds/sql-lint/releases
```bash
wget https://github.com/joereynolds/sql-lint/releases/download/v1.0.0/sql-lint-linux
sudo mv sql-lint-linux /usr/local/bin/sql-lint
chmod +x /usr/local/bin/sql-lint
```
Would be good for MySQL, but v1.0 finding errors where it should not...


### phpcompatinfo
https://github.com/llaville/php-compat-info/
https://llaville.github.io/php-compatinfo/6.x/
Where are the PHAR now?
```bash
cd qa
wget -O phpcompatinfo.phar http://bartlett.laurent-laville.org/get/phpcompatinfo-5.2.1.phar
php phpcompatinfo.phar
php phpcompatinfo.phar analyser:run --output=../public/phpcompatinfo.txt --alias rosariosis
```

### PHP Mess Detector
https://github.com/phpmd/phpmd/
https://phpmd.org/rules/index.html
```bash
cd qa
wget -O phpmd.phar https://static.phpmd.org/php/latest/phpmd.phar
php phpmd.phar
php phpmd.phar ../../functions/ html codesize > ../public/phpmd_functions_codesize.html
php phpmd.phar ../../modules/ html codesize > ../public/phpmd_modules_codesize.html
php phpmd.phar ../../functions/ html unusedcode > ../public/phpmd_functions_unusedcode.html
php phpmd.phar ../../modules/ html unusedcode > ../public/phpmd_modules_unusedcode.html
php phpmd.phar ../../functions/ html cleancode > ../public/phpmd_functions_cleancode.html
```

### PHP Copy/Paste Detector
https://github.com/sebastianbergmann/phpcpd/

Note: version 4.1.0 requires PHP >= 7.1.
```bash
cd qa
wget -O phpcpd.phar https://phar.phpunit.de/phpcpd-3.0.1.phar
php phpcpd.phar
php phpcpd.phar --exclude=classes --exclude=node_modules --exclude=meta ../.. > ../public/phpcpd.txt
```

### PhpMetrics
https://github.com/phpmetrics/PhpMetrics/

```bash
cd qa
wget https://github.com/phpmetrics/PhpMetrics/releases/download/v2.4.1/phpmetrics.phar
php phpmetrics.phar --report-html=../public/phpmetrics/ --exclude=node_modules,classes,meta,API,Moodle_TinyMCE_RecordRTC ../..
```
Only analyses classes, not functions...

### PHP Lines of Code
https://github.com/sebastianbergmann/phploc

```bash
wget https://phar.phpunit.de/phploc.phar
php phploc.phar --exclude=classes --exclude=meta --exclude=Moodle_TinyMCE_RecordRTC ../.. > ../public/phploc.txt
```

### PHP Static Analysis Tool
https://github.com/phpstan/phpstan/

```bash
cd qa
wget https://github.com/phpstan/phpstan/releases/download/2.0.2/phpstan.phar
php phpstan.phar analyse --configuration phpstan.neon > ../public/phpstan.txt
```

### PHP static analyzer
https://github.com/phan/phan

```bash
cd qa
wget https://github.com/phan/phan/releases/download/2.0.0/phan.phar
php phan.phar --allow-polyfill-parser --directory=../.. ../..
```


## Refactoring

### Rector
https://github.com/rectorphp/rector

```bash
cd refactoring
composer require rector/rector --dev
vendor/bin/rector process --dry-run
```
See `rector.php` file for configuration.

Add a new rule:
https://github.com/rectorphp/rector/blob/main/docs/create_own_rule.md
```bash
composer dump-autoload
```

### EasyCodingStandard
https://github.com/symplify/easy-coding-standard

```bash
cd refactoring
composer require symplify/easy-coding-standard --dev
vendor/bin/ecs check
```
See `ecs.php` file for configuration.

Used by Rector for Coding Standard (or supposedly so...)!

See https://github.com/FriendsOfPHP/PHP-CS-Fixer for PHP CS Fixer rules.


## Profiler

### Webgrind
https://github.com/jokkedk/webgrind
```bash
cd qa
wget -O webgrind.tar.gz https://github.com/jokkedk/webgrind/archive/v1.9.3.tar.gz
tar -xvzf webgrind.tar.gz
rm webgrind.tar.gz
mv webgrind-1.9.3 webgrind
```
https://xdebug.org/docs/profiler
Make sure PHP `xdebug` extension is activated and add to `php.ini`:
```
; Webgrind (Xdebug profiling)
xdebug.mode = develop,profile
xdebug.profiler_enable = 1
xdebug.profiler_output_dir = /tmp
xdebug.use_compression  = false
xdebug.profiler_output_name= "cachegrind.out.%s"
```
Got to http://localhost/rosariosis/meta/qa/webgrind/


## Security

### sqlmap
http://sqlmap.org/
Automatic SQL injection and database takeover tool
```bash
sudo apt install sqlmap
sqlmap -h
sudo chmod +x security-run.sh
./security-run.sh "http://localhost/gitlab/rosariosis"
```

### phpcs-security-audit
https://github.com/FloeDesignTechnologies/phpcs-security-audit/
phpcs-security-audit is a set of PHP_CodeSniffer rules that finds vulnerabilities and weaknesses related to security in PHP code
```bash
cd security
git clone https://github.com/FloeDesignTechnologies/phpcs-security-audit.git
cd phpcs-security-audit/
composer require dealerdirect/phpcodesniffer-composer-installer
./vendor/bin/phpcs -i
./vendor/bin/phpcs \
  --ignore=*/meta/*,*/node_modules/*,*/PHPExcel/*,*/vendor/*,*/gateways/*,*/PHPCompatibility/* \
  --runtime-set ParanoiaMode 0 --extensions=php --standard=./example_base_ruleset.xml ../../../ > result.md
```


## log analysis

### lnav
https://lnav.org/
An advanced log file viewer for the small-scale
```bash
sudo apt install lnav
```

```bash
lnav access
```
20 most used programs:
```bash
lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" GROUP BY cs_uri_query ORDER BY total DESC LIMIT 20' -c ':write-csv-to 2019_demo_20_most_used_programs.csv' logs/2019.02/access
```
50 most used School Setup programs:
```bash
lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=School_Setup%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 50' -c ':write-csv-to 2020_demo_50_most_used_School_Setup_programs.csv' logs/2020.02/access
```

```bash
bash ./lnav_access_log.sh
```

## Demo data

- Demonstration SQL files can be found inside the [`demo/`](https://gitlab.com/francoisjacquet/rosariosis-meta/-/tree/master/demo) folder.
- Run the SQL file after RosarioSIS install.
- Requires the [Messaging](https://www.rosariosis.org/modules/messaging/) module.
- Login with `administrator` as login and password, just like on the [demo](https://www.rosariosis.org/demo/) site.


## SQL scripts

Inside the `sql/` folder:
- PostgreSQL script to undo _Rollover_, in case it was accidentally run during (and not at the end of) the school year


## TODO

- PHPDoc
https://github.com/phpDocumentor/phpDocumentor2

See PHP_QA.txt
