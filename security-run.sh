#!/bin/bash

# http://www.sqlinjection.net/sqlmap/tutorial/
# http://www.darkmoreops.com/2014/08/28/use-sqlmap-sql-injection-hack-website-database/

echo "Security run: $1"

cd security

echo "sqlmap"

echo "Login POST..."
sqlmap --random-agent --level=2 --risk=3 --batch -v 0 --dbms=pgsql \
	-u "${1}/" --data="USERNAME=admin&PASSWORD=admin"

echo "Public Page Marking Periods GET..."
sqlmap --random-agent --level=2 --risk=3 --batch -v 0 --dbms=pgsql \
	-u "${1}/index.php?public-page=markingperiods&modfunc=&mp_term=QTR&year_id=1&semester_id=2&marking_period_id=4"

printf " quote..."
sqlmap --random-agent --level=2 --risk=3 --batch -v 0 --dbms=pgsql \
	-u "${1}/index.php?public-page=markingperiods&modfunc=&mp_term=QTR&year_id=1&semester_id=2&marking_period_id=4'"

echo "Public Page Calendars GET..."
sqlmap --random-agent --level=2 --risk=3 --batch -v 0 --dbms=pgsql \
	-u "${1}/index.php?public-page=calendar&calendar_id=1"

printf " quote..."
sqlmap --random-agent --level=2 --risk=3 --batch -v 0 --dbms=pgsql \
	-u "${1}/index.php?public-page=calendar&calendar_id=1'"

echo "phpcs-security-audit"

phpcs-security-audit/vendor/bin/phpcs \
  --ignore=*/meta/*,*/node_modules/*,*/PHPExcel/*,*/vendor/*,*/gateways/*,*/PHPCompatibility/* \
  --runtime-set ParanoiaMode 0 --extensions=php --standard=phpcs-security-audit_ruleset.xml ../../../ > ../public/phpcs-security-audit.md

echo "Done!"
