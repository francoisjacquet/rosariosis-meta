#!/bin/bash

echo "QA run"

cd qa

echo "lint..."
lint --exclude=node_modules,classes,PHPExcel,meta,vendor ../.. > ../public/lint.txt

if [[ -e /usr/bin/php5.6 ]]; then
	echo "lint PHP5.6..."
	/usr/bin/php5.6 /usr/local/bin/lint --exclude=node_modules,classes,PHPExcel,SimpleXLSX,meta,vendor ../.. > ../public/lint_php5.6.txt
else
	echo "PHP5.6 not installed!"
fi

echo "sqlint..."
# Exclude MySQL files, sqlint cannot parse MySQL syntax, alternative?
find ../.. -maxdepth 3 -type f -name "*.sql" ! -name "*_mysql.sql" -exec cat {} \; > all_sql
sqlint all_sql > ../public/sqlint.txt
rm all_sql

# MySQL files, sql-lint can parse MySQL, NOPE, v1.0 finding errors where it should not
#find ../.. -maxdepth 3 -type f -name "*_mysql.sql" -exec cat {} \; > all_mysql
#sql-lint --ignore-errors=trailing-whitespace all_mysql > ../public/sql-lint.txt
#rm all_mysql



echo "phpmd..."
php phpmd.phar ../../functions/ html codesize --reportfile ../public/phpmd_functions_codesize.html --ignore-violations-on-exit

printf "."
php phpmd.phar ../../ProgramFunctions/ html codesize --reportfile ../public/phpmd_programfunctions_codesize.html --ignore-violations-on-exit &

printf "."
php phpmd.phar ../../modules/ html codesize \
  --exclude Students_Import/classes/SimpleXLSX/*,Staff_Parents_Import/classes/SimpleXLSX/*,Timetable_Import/classes/SimpleXLSX/*,Attendance_Excel_Sheet/classes/PHPExcel/*,ENP/classes/SimpleXLSX/*,Student_Billing_Premium/classes/SimpleXLSX/*,SMS_Premium/vendor/*,SMS_Premium/includes/gateways/*,SMS/includes/gateways/*,Dashboards/css-sanitizer* \
  --reportfile ../public/phpmd_modules_codesize.html --ignore-violations-on-exit &

printf "."
php phpmd.phar ../../functions/ html unusedcode --reportfile ../public/phpmd_functions_unusedcode.html --ignore-violations-on-exit &

printf "."
php phpmd.phar ../../ProgramFunctions/ html unusedcode --reportfile ../public/phpmd_programfunctions_unusedcode.html --ignore-violations-on-exit &

printf "."
php phpmd.phar ../../modules/ html unusedcode \
  --exclude Students_Import/classes/SimpleXLSX/*,Staff_Parents_Import/classes/SimpleXLSX/*,Timetable_Import/classes/SimpleXLSX/*,Attendance_Excel_Sheet/classes/PHPExcel/*,ENP/classes/SimpleXLSX/*,Student_Billing_Premium/classes/SimpleXLSX/*,SMS_Premium/vendor/*,SMS_Premium/includes/gateways/*,SMS/includes/gateways/*,Dashboards/css-sanitizer* \
  --reportfile ../public/phpmd_modules_unusedcode.html --ignore-violations-on-exit &

echo "."
php phpmd.phar ../../functions/ html cleancode --reportfile ../public/phpmd_functions_cleancode.html --ignore-violations-on-exit &

echo "."
php phpmd.phar ../../ProgramFunctions/ html cleancode --reportfile ../public/phpmd_programfunctions_cleancode.html --ignore-violations-on-exit &

echo "phpcpd..."
php phpcpd.phar --exclude="../../modules/*/classes/PHPExcel/" --exclude="../../modules/*/classes/SimpleXLSX/" --exclude="../../modules/*/DBUpsert.php" --exclude="../../meta/" --exclude="../../*/*/vendor/"  --exclude="../../*/*/*/vendor/" --exclude="../../modules/*/includes/gateways/" ../.. > ../public/phpcpd.txt

echo "phploc..."
php phploc.phar --exclude="../../modules/*/classes/PHPExcel/" --exclude="../../modules/*/classes/SimpleXLSX/" --exclude="../../meta/" --exclude="../../*/*/vendor/" --exclude="../../*/*/*/vendor/" --exclude="../../modules/*/includes/gateways/" ../.. > ../public/phploc.txt

echo "phpcompatinfo..."
php phpcompatinfo.phar analyser:run --output=../public/phpcompatinfo.txt --alias rosariosis

echo "phpmetrics (deactivated)"
#php phpmetrics.phar --report-html=../public/phpmetrics/ --exclude=node_modules,classes,meta,API ../..

echo "Done!"

echo "Run wappstack-lint.bat on Windows"
