#!/bin/bash

echo "Test group run: $1"

# Empty previous tests output.
rm public/tests/_output/*

chromedriver --url-base=/wd/hub &

php codecept.phar run -g $1 --steps --debug --html

pkill chromedriver
