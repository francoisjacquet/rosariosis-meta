#!/bin/bash

# Analyze access logs using lnav
# Place it below the access/ folder
# ${YEAR}_demo_20_most_used_programs.csv
# ${YEAR}_demo_50_most_used_School_Setup_programs.csv
# ${YEAR}_demo_20_most_used_Accounting_programs.csv
# ...

# Ask for year
printf "Year: "
read YEAR

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" GROUP BY cs_uri_query ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=School_Setup%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 50' -c ":write-csv-to ${YEAR}_demo_50_most_used_School_Setup_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Accounting%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Accounting_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Attendance%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Attendance_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Custom%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Custom_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Discipline%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Discipline_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Eligibility%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Eligibility_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Food_Service%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Food_Service_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Grades%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Grades_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Scheduling%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Scheduling_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Student_Billing%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Student_Billing_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Students%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Students_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND cs_uri_query LIKE "modname=Users%" GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_used_Users_programs.csv" access

lnav -n -c ';SELECT cs_uri_query, count(*) AS total FROM access_log WHERE cs_uri_stem="/demonstration/Modules.php" AND (cs_uri_query LIKE "%modfunc=activate&module=%" OR cs_uri_query LIKE "%modfunc=activate&plugin=%") GROUP BY cs_uri_query HAVING total>1 ORDER BY total DESC LIMIT 20' -c ":write-csv-to ${YEAR}_demo_20_most_activated_addons.csv" access
