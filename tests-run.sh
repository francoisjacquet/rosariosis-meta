#!/bin/bash

echo "Tests run"

# Empty previous tests output.
rm public/tests/_output/*

chromedriver --url-base=/wd/hub --port=9515 &

php codecept.phar run --steps --debug --html

pkill chromedriver
