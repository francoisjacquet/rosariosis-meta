<?php
/**
 * Create Student Account - Index tests
 *
 * $ ./test-run.sh tests/acceptance/IndexCreateStudentAccountCest.php
 */

require_once 'IndexCreateAccount.php';

class IndexCreateStudentAccountCest extends IndexCreateAccount
{
	public function _before(AcceptanceTester $I)
	{
	}

	// Tests.
	public function activate(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		if ( $I->seePageHasLink( 'Create Student Account' ) )
		{
			return;
		}

		$I->login( 'admin' );

		$I->amOnPage( '/Modules.php?modname=School_Setup/Configuration.php&tab=system' );

		$I->click( '#divvaluesconfigCREATE_STUDENT_ACCOUNT' );

		$I->checkOption( '#valuesconfigCREATE_STUDENT_ACCOUNT' );

		$I->save();

		$I->logout();
	}

	public function click(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		$I->click( 'Create Student Account' );

		$I->seeInCurrentUrl( 'index.php?create_account=student&student_id=new' );

		$I->cantSee( 'Create Student in Moodle' );

		$I->see( 'Captcha' );
	}

	public function create(AcceptanceTester $I)
	{
		$this->first_name = 'Test';

		$this->middle_name = 'Codeception\'';

		$this->last_name = date( 'Y-m-d H:i:s' );

		$this->name = $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;

		$I->fillField( 'First Name', $this->first_name );

		$I->fillField( 'Middle Name', $this->middle_name );

		$I->fillField( 'Last Name', $this->last_name );

		$I->fillField( 'Username', $this->getTestUsernamePassword( $I ) );

		$I->fillField( 'Password', $this->getTestUsernamePassword( $I ) );

		$I->fillField( 'Captcha', $this->getCatpcha( $I ) );

		// Fix Do not use $I->save() here!
		// $I->save();
		$I->click( 'Save' );

		$I->waitForElement( '#loginform', 3 );

		$I->seeInCurrentUrl( '&reason=account_created' );
	}

	public function login(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		// Logging in.
		$I->submitForm( '#loginform',
			array(
				'USERNAME' => $this->getTestUsernamePassword( $I ),
				'PASSWORD' => $this->getTestUsernamePassword( $I ),
			)
		);

		$I->see( 'Your account has not yet been activated.', '.error' );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->login( 'admin' );

		$I->amOnPage( '/Modules.php?modname=Students/Student.php' );

		$I->checkOption( 'Include Inactive Students' );

		$I->search( $this->name );

		$I->delete();
	}
}
