<?php

class IndexCest
{
	public function _before(AcceptanceTester $I)
	{
	}

	// Tests.
	public function seeLoginForm(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		// @link https://codeception.com/docs/modules/WebDriver#seeInFormFields
		$I->seeInFormFields( 'form[name="loginform"]',
			array(
				'USERNAME' => '',
				'PASSWORD' => '',
			)
		);

	}

	public function clickFrenchFlag(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		$I->click( 'French' );

		$I->seeInCurrentUrl( 'index.php?locale=fr_FR.utf8' );

		$I->cantSeeLink( 'Password help', 'PasswordReset.php' );

		$I->canSeeLink( 'Aide mot de passe', 'PasswordReset.php' );
	}

	public function clickSpanishFlag(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		$I->click( 'Spanish' );

		$I->seeInCurrentUrl( 'index.php?locale=es_ES.utf8' );

		$I->cantSeeLink( 'Password help', 'PasswordReset.php' );

		$I->canSeeLink( 'Ayuda Contraseña', 'PasswordReset.php' );
	}
}
