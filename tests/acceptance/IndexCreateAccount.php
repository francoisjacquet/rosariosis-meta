<?php

class IndexCreateAccount
{
	// Protected.
	protected function getTestUsernamePassword(AcceptanceTester $I)
	{
		static $username;

		if ( ! $username )
		{
			$username = 'test@' . date( 'Y-m-d-H-i-s' ) . '.com';

			$I->am( 'generating username & password: ' . $username );
		}

		return $username;
	}

	protected function getCatpcha(AcceptanceTester $I)
	{
		$captcha1 = $I->grabTextFrom( '.captcha > span:first-of-type' );

		$captcha2 = $I->grabTextFrom( '.captcha > span:nth-of-type(2)' );

		$I->expect( 'Captcha value to be: ' . $captcha1 . ' + ' . $captcha2 );

		return $captcha1 + $captcha2;
	}
}
