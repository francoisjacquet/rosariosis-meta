<?php
/**
 * ReportCardComments - Grades tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Grades/ReportCardCommentsCest.php
 */

class ReportCardCommentsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardComments.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	// TODO
	/*public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/Student.php&include=General_Info&student_id=new' );

		$I->selectOption( '#valuesSTUDENT_ENROLLMENTnewENROLLMENT_CODE', $this->title );
	}*/

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardComments.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}

	public function addAllCourses(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardComments.php&tab_id=0' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	// TODO
	/*public function checkAddAllCourses(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/Student.php&include=General_Info&student_id=0' );

		$I->selectOption( '#valuesSTUDENT_ENROLLMENTnewENROLLMENT_CODE', $this->title );
	}*/

	public function deleteAllCourses(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardComments.php&tab_id=0' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
