<?php
/**
 * ReportCardCommentCodes - Grades tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Grades/ReportCardCommentCodesCest.php
 */

class ReportCardCommentCodesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardCommentCodes.php&tab_id=new' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewCOMMENT', 'Test1' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardComments.php&tab_id=0' );

		$I->selectOption( '#valuesnewSCALE_ID', $this->title );
	}

	public function addComment(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardCommentCodes.php' );

		$this->titleComment = 'Tst1\'';

		$I->fillField( '#valuesnewTITLE', $this->titleComment );

		$I->fillField( '#valuesnewCOMMENT', 'Test Comment 1' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->titleComment );
	}

	public function deleteComment(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardCommentCodes.php' );

		$I->remove();

		$I->cantSee( $this->titleComment );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardCommentCodes.php&tab_id=new' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
