<?php
/**
 * ReportCardGrades - Grades tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Grades/ReportCardGradesCest.php
 */

class ReportCardGradesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardGrades.php' );

		$this->title = 'Tst1\'';

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewBREAK_OFF', '100' );

		$I->fillField( '#valuesnewGPA_VALUE', '100' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	// TODO
	/*public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/Student.php&include=General_Info&student_id=new' );

		$I->selectOption( '#valuesSTUDENT_ENROLLMENTnewENROLLMENT_CODE', $this->title );
	}*/

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardGrades.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}

	public function addScale(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardGrades.php&tab_id=new' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewGP_SCALE', '100' );

		$I->fillField( '#valuesnewGP_PASSING_VALUE', '50' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	// TODO
	/*public function checkAddScale(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/Student.php&include=General_Info&student_id=new' );

		$I->selectOption( '#valuesSTUDENT_ENROLLMENTnewENROLLMENT_CODE', $this->title );
	}*/

	public function deleteScale(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/ReportCardGrades.php&tab_id=new' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
