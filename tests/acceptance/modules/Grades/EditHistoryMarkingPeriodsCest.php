<?php
/**
 * EditHistoryMarkingPeriods - Grades tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Grades/EditHistoryMarkingPeriodsCest.php
 */

class EditHistoryMarkingPeriodsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/EditHistoryMarkingPeriods.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$this->syear = ( date( 'Y' ) - 2 ) . '-' . ( date( 'Y' ) - 1 );

		$I->fillField( '#valuesnewNAME', $this->title );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test1' );

		$I->selectDate( 'values[new][POST_END_DATE]', '12', '20', ( date( 'Y' ) - 2 ) );

		$I->selectOption( '#valuesnewSYEAR', $this->syear );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/EditReportCardGrades.php' );

		$I->search( 'Student S Student' );

		$I->selectOption( 'select[name="mp_id"]', 'Add another marking period' );

		$I->waitForAJAX();

		$I->selectOption( 'New Marking Period', ( $this->syear . ', ' . $this->title ) );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Grades/EditHistoryMarkingPeriods.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
