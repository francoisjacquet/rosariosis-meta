<?php
/**
 * Activities - Eligibility tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Eligibility/ActivitiesCest.php
 */

class ActivitiesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Eligibility/Activities.php' );

		$this->title = '1Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->selectDate( 'values[new][START_DATE]', '06', '15', date( 'Y' ) );

		$I->selectDate( 'values[new][END_DATE]', '08', '15', date( 'Y' ) );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Eligibility/AddActivity.php' );

		$I->search( 'Student S Student' );

		$I->selectOption( 'Activity', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Eligibility/Activities.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
