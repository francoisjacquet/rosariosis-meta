<?php
/**
 * StudentPayments - Student_Billing tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Student_Billing/StudentPaymentsCest.php
 */

class StudentPaymentsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Student_Billing/StudentPayments.php' );

		$I->search( 'Student S Student' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewCOMMENTS', $this->title );

		$I->fillField( '#valuesnewAMOUNT', '100.56' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Student_Billing/StudentPayments.php' );

		// Prevent from clicking on Refund!
		$I->click( 'a[href^="Modules.php?modname=Student_Billing/StudentPayments.php&modfunc=remove&id="]' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->cantSee( $this->title );
	}
}
