<?php
/**
 * Student Fees - Student Billing tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Student_Billing/StudentFeesCest.php
 */

class StudentFeesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
	}

	// Tests.
	public function try(AcceptanceTester $I)
	{
		$I->login( 'admin' );

		$I->amOnPage( '/Modules.php?modname=Student_Billing/StudentFees.php' );

		$I->search( 'Student S Student' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$this->amount = '200.50';

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewAMOUNT', $this->amount );

		$I->save();

		$I->see( $this->title );
	}

	public function parentPortalNote(AcceptanceTester $I)
	{
		$I->login( 'parent' );

		// $I->click( '1 new fee' );

		$I->amOnPage( '/Modules.php?modname=Student_Billing/StudentFees.php' );

		$I->see( $this->title );

		$I->see( $this->amount );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->login( 'admin' );

		$I->amOnPage( '/Modules.php?modname=Student_Billing/StudentFees.php' );

		$I->search( 'Student S Student' );

		// Prevent from clicking on Waive!
		$I->click( 'a[href^="Modules.php?modname=Student_Billing/StudentFees.php&modfunc=remove&id="]' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->cantSee( $this->title );
	}
}
