<?php
/**
 * Menus - Food_Service tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Food_Service/MenusCest.php
 */

class MenusCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/Menus.php&tab_id=new' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php' );

		$I->canSee( $this->title );
	}

	public function addCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/Menus.php' );

		$this->titleCategory = 'Test1\'';

		$I->fillField( '#valuesnewTITLE', $this->titleCategory );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->titleCategory );
	}

	public function checkAddCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php' );

		$I->selectOption( '#valuesnewCATEGORY_ID', $this->titleCategory );
	}

	public function deleteCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/Menus.php' );

		$I->remove();

		$I->cantSee( $this->titleCategory );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/Menus.php&tab_id=new' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
