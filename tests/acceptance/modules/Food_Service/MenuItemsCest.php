<?php
/**
 * MenuItems - Food_Service tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Food_Service/MenuItemsCest.php
 */

class MenuItemsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php&tab_id=new' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewDESCRIPTION', $this->title );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test1' );

		$I->fillField( '#valuesnewPRICE', '10' );

		$I->fillField( '#valuesnewPRICE_STAFF', '20.56' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function addItem(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php' );

		$I->click( 'Lunch' );

		$I->waitForAJAX();

		$I->selectOption( '#valuesnewITEM_ID', $this->title );

		$I->selectOption( '#valuesnewCATEGORY_ID', 'Lunch Items' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAddItem(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/ServeMenus.php' );

		$I->search( 'Student S Student' );

		$I->click( 'Lunch' );

		$I->waitForAJAX();

		$I->selectOption( '#item_sn', $this->title );
	}

	// Delete function below will also delete Item.
	/*public function deleteItem(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}*/

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Food_Service/MenuItems.php&tab_id=new' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
