<?php
/**
 * Salaries - Accounting tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Accounting/SalariesCest.php
 */

class SalariesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Accounting/Salaries.php' );

		$I->search( 'Admin A Administrator', 'self' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewAMOUNT', '100.56' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Accounting/Salaries.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
