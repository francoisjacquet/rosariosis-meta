<?php
/**
 * Incomes - Accounting tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Accounting/IncomesCest.php
 */

class IncomesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Accounting/Incomes.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewAMOUNT', '100.56' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Accounting/Incomes.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
