<?php
/**
 * EnrollmentCodes - Students tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Students/EnrollmentCodesCest.php
 */

class EnrollmentCodesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/EnrollmentCodes.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test1' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/Student.php&include=General_Info&student_id=new' );

		$I->selectOption( '#valuesstudent_enrollmentnewENROLLMENT_CODE', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/EnrollmentCodes.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
