<?php
/**
 * ContactFields - Students tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Students/ContactFieldsCest.php
 */

class ContactFieldsCest
{
	private $title;

	private $category;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.Student
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/StudentFields.php' );

		$I->selectOption( 'Category', 'Contact Fields' );

		$I->waitForAJAX();

		$I->click( 'Add' );

		$I->waitForAJAX();

		$this->category = 'Category\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#ML_tablesnewTITLEen_USutf8', $this->category );

		$I->save();

		$I->click( $this->category );

		$I->waitForAJAX();

		$I->click( '.list-outer.fields img[alt="Add"]' );

		$I->waitForAJAX();

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#ML_tablesnewTITLEen_USutf8', $this->title );

		$I->fillField( '#tablesnewSELECT_OPTIONS', "TestOption1\nTestOption2\nTestOption3" );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/Student.php' );

		$I->search( 'Student S Student' );

		// $I->click( 'Addresses & Contacts' ); // Not found...
		$I->amOnPage( '/Modules.php?modname=Students/Student.php&category_id=3' );

		$I->click( 'No Address' );

		$I->waitForAJAX();

		$I->click( 'Add a New Contact' );

		$I->waitForAJAX();

		$I->canSee( $this->category );

		$I->selectOption( $this->title, 'TestOption2' );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/StudentFields.php' );

		$I->selectOption( 'Category', 'Contact Fields' );

		$I->waitForAJAX();

		$I->click( $this->category );

		$I->waitForAJAX();

		$I->click( $this->title );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title );

		$I->click( $this->category );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->category );
	}
}
