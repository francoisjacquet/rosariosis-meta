<?php
/**
 * Medical inc - Students tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Students/includes/MedicalincCest.php
 */

class MedicalincCest
{
	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function seeTab(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/Student.php' );

		$I->search( 'Student S Student' );

		$I->click( 'Medical' );

		$I->waitForAJAX();
	}

	public function addImmunization(AcceptanceTester $I)
	{
		$I->selectOption( '#valuesstudent_medicalnewTYPE', 'Immunization' );

		$I->selectDate( 'values[student_medical][new][MEDICAL_DATE]' );

		$comments = 'Com Immunization\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesstudent_medicalnewCOMMENTS', $comments );

		$I->save();

		$I->waitForAJAX();

		$I->see( $comments );
	}

	public function addPhysical(AcceptanceTester $I)
	{
		$I->selectOption( '#valuesstudent_medicalnewTYPE', 'Physical' );

		$I->selectDate( 'values[student_medical][new][MEDICAL_DATE]' );

		$comments = 'Com Physical\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesstudent_medicalnewCOMMENTS', $comments );

		// Fix error Save not clickable because not visible...
		$I->scrollTop();

		$I->save();

		$I->see( $comments );
	}

	public function searchImmunizationOrPhysical(AcceptanceTester $I)
	{
		$I->click( 'Clear working student' );

		$I->waitForAJAX();

		$I->click( 'Advanced Search' );

		$I->waitForAJAX();

		$I->scrollBottom( 200 );

		$I->click( 'Medical' );

		$I->selectDate( 'medical_begin' );

		$I->scrollTop();

		$I->click( 'Submit' );

		$I->waitForAJAX();

		$I->see( 'Student S Student' );
	}

	public function deleteImmunization(AcceptanceTester $I)
	{
		$I->click( 'Student S Student' );

		$I->waitForAJAX();

		$I->click( 'Medical' );

		$I->waitForAJAX();

		$I->click( 'Remove', '.immunizations-or-physicals .list' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();
	}

	public function deletePhysical(AcceptanceTester $I)
	{
		$I->click( 'Remove', '.immunizations-or-physicals .list' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();
	}
}
