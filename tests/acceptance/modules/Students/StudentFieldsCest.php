<?php
/**
 * StudentFields - Students tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Students/StudentFieldsCest.php
 */

class StudentFieldsCest
{
	private $title;

	private $category;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.Student
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/StudentFields.php' );

		$I->click( 'Add' );

		$I->waitForAJAX();

		$this->category = 'Category\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#ML_tablesnewTITLEen_USutf8', $this->category );

		$I->save();

		$I->canSee( $this->category );

		$I->click( 'General Info' );

		$I->waitForAJAX();

		$I->amOnPage( '/Modules.php?modname=Students/StudentFields.php&category_id=1&id=new' );

		$I->waitForAJAX();

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#ML_tablesnewTITLEen_USutf8', $this->title );

		$I->fillField( '#tablesnewSELECT_OPTIONS', "TestOption1\nTestOption2\nTestOption3" );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/Student.php' );

		$I->search( 'Student S Student' );

		$I->canSee( $this->category );

		$I->selectOption( $this->title, 'TestOption2' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->canSee( 'TestOption2' );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/StudentFields.php' );

		$I->click( $this->category );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->category );

		$I->click( 'General Info' );

		$I->waitForAJAX();

		$I->click( $this->title );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title );
	}
}
