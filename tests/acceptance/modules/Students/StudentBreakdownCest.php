<?php
/**
 * StudentBreakdown - Students tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Students/StudentBreakdownCest.php
 */

class StudentBreakdownCest
{
	private $option;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function selectGradeLevel(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/StudentBreakdown.php' );

		$this->option = 'Grade Level';

		$I->selectOption( 'select[name="field_id"]', $this->option );

		$I->waitForAJAX();

		$I->seeOptionIsSelected( 'select[name="field_id"]', $this->option );

		$I->seeElement( '#chart0' );
	}

	public function advanced(AcceptanceTester $I)
	{
		$I->click( 'Advanced' );

		$I->waitForAJAX();

		$I->fillField( 'RosarioSIS ID', '1' );

		$I->click( 'Submit' );

		$I->waitForAJAX();

		$I->see( 'RosarioSIS ID: 1' );

		$I->seeOptionIsSelected( 'select[name="field_id"]', $this->option );

		$I->seeElement( '#chart0' );
	}
}
