<?php
/**
 * Profiles - Users tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Users/ProfilesCest.php
 */

class ProfilesCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function try(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/Profiles.php' );

		$I->click( 'Add a User Profile' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( 'Title', $this->title );

		$I->save();

		$I->see( $this->title );
	}

	public function available(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/User.php' );

		$I->search( 'Admin A Administrator', 'self' );

		$I->click( '#divstaffPROFILE_ID > .onclick' );

		$I->selectOption( '#staffPROFILE_ID', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/Profiles.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
