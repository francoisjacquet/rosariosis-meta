<?php
/**
 * Preferences - Users tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Users/PreferencesCest.php
 */

class PreferencesCest
{
	private $date;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function passwordTabSelected(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/Preferences.php' );

		$I->see( 'Password', 'h3.h3selected' );
	}

	public function changeDateFormat(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/Preferences.php&tab=display_options' );

		$this->date = date( 'Y-m-d' );

		$I->selectOption( '#valuesPreferencesDATE', $this->date );

		$I->save();

		$I->seeOptionIsSelected( '#valuesPreferencesDATE', $this->date );
	}

	public function checkDateFormat(AcceptanceTester $I)
	{
		$I->reloadPage();

		// Full date in Side menu.
		$I->see( date( 'l ' ) . $this->date, '.today-date' );
	}

	public function changeBackDateFormat(AcceptanceTester $I)
	{
		$this->date = strftime( '%B %d %Y' );

		$I->selectOption( '#valuesPreferencesDATE', $this->date );

		$I->save();

		$I->seeOptionIsSelected( '#valuesPreferencesDATE', $this->date );
	}
}
