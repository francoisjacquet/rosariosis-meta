<?php
/**
 * Courses - Scheduling tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Scheduling/CoursesCest.php
 * $ ./test-group-run.sh course
 */

/**
 * @group course
 */
class CoursesCest
{
	private $title_subject;
	private $title_course;
	private $title_course_period;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function addSubject(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Scheduling/Courses.php' );

		$I->click( 'Add' );

		$I->waitForAJAX();

		$this->title_subject = '1Test\' ' . date( 'Y-m-d' ) . ' Subject';

		if ( $I->seePageHasElement( '#ML_tablescourse_subjectsnewTITLEen_USutf8' ) )
		{
			// Multilang courses.
			$I->fillField( '#ML_tablescourse_subjectsnewTITLEen_USutf8', $this->title_subject );
		}
		else
		{
			$I->fillField( '#tablescourse_subjectsnewTITLE', $this->title_subject );
		}

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#tablescourse_subjectsnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title_subject );
	}

	public function addCourse(AcceptanceTester $I)
	{
		$I->click( 'Add', '.courses' );

		$I->waitForAJAX();

		$this->title_course = '1Test\' ' . date( 'Y-m-d' ) . ' Course';

		if ( $I->seePageHasElement( '#ML_tablescoursesnewTITLEen_USutf8' ) )
		{
			// Multilang courses.
			$I->fillField( '#ML_tablescoursesnewTITLEen_USutf8', $this->title_course );
		}
		else
		{
			$I->fillField( '#tablescoursesnewTITLE', $this->title_course );
		}

		$I->fillField( '#tablescoursesnewCREDIT_HOURS', '9999' );

		$I->save();

		$I->canSee( $this->title_course );
	}

	public function addCoursePeriod(AcceptanceTester $I)
	{
		$I->click( 'Add', '.course-periods' );

		$I->waitForAJAX();

		$this->title_course_period = '1Test\' ' . date( 'Y-m-d' ) . ' CP';

		$I->fillField( '#tablescourse_periodsnewSHORT_NAME', $this->title_course_period );

		$I->selectOption( '#tablescourse_periodsnewTEACHER_ID', 'Teach T Teacher' );

		$I->fillField( '#tablescourse_periodsnewTOTAL_SEATS', '9999' );

		$I->selectOption( '#tablescourse_period_school_periodsnew1PERIOD_ID', 'Period 8' );

		$I->checkOption( 'Attendance' );

		$I->save();

		$I->canSee( $this->title_course_period );
	}
}
