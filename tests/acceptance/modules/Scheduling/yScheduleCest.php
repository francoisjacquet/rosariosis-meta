<?php
/**
 * Schedule - Scheduling tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Scheduling/yScheduleCest.php
 * $ ./test-group-run.sh course
 */

/**
 * @group course
 */
class yScheduleCest
{
	private $title_course;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function dropCourse(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Scheduling/Schedule.php' );

		$I->search( 'Student S Student' );

		$this->title_course = '1Test\' ' . date( 'Y-m-d' ) . ' Course';

		$I->canSee( $this->title_course );

		$name = $I->executeJS( "return $('select[name^=month_schedule]').first().attr('name').replace('month_', '');" );

		// Select yesterday's date as Dropped date.
		$I->selectDate(
			$name,
			date( 'F' ),
			date( 'd', time() - 60 * 60 * 24 ),
			date( 'Y' )
		);

		$I->click( 'Save' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->cantSee( $this->title_course );
	}
}
