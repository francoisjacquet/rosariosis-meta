<?php
/**
 * Schedule - Scheduling tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Scheduling/ScheduleCest.php
 * $ ./test-group-run.sh course
 */

/**
 * @group course
 */
class ScheduleCest
{
	private $title_subject;
	private $title_course;
	private $title_course_period;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function scheduleCourse(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Scheduling/Schedule.php' );

		$I->search( 'Student S Student' );

		$I->click( 'Add a Course' );

		// Switch to popup.
		// Commented out, popup is not anymore a window but now a colorBox.
		// $I->switchToNextTab();

		$I->waitForAJAX();

		$I->wait( 1 ); // 1 second.

		$this->title_subject = '1Test\' ' . date( 'Y-m-d' ) . ' Subject';

		$this->title_course = '1Test\' ' . date( 'Y-m-d' ) . ' Course';

		$this->title_course_period = '1Test\' ' . date( 'Y-m-d' ) . ' CP';

		$I->click( $this->title_subject );

		$I->waitForAJAX();

		$I->click( $this->title_course );

		$I->waitForAJAX();

		$I->click( $this->title_course_period );

		// Popup should now be closed.
		$I->switchToPreviousTab();

		$I->waitForText($this->title_course, 4);
	}
}
