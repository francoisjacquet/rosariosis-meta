<?php
/**
 * TakeAttendance - Scheduling tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Scheduling/TakeAttendanceCest.php
 * $ ./test-group-run.sh course
 */

/**
 * @group course
 */
class TakeAttendanceCest
{
	private $title_course_period;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function checkStudent(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/TeacherPrograms.php&include=Attendance/TakeAttendance.php' );

		$I->search( 'Teach T Teacher', 'staff' );

		$this->title_course_period = '1Test\' ' . date( 'Y-m-d' ) . ' CP';

		$I->selectOption( '#period', $this->title_course_period );

		$I->waitForAJAX();

		if ( date( 'w' ) > 5 || date( 'w' ) < 1 )
		{
			// Is Saturday or Sunday, select next Monday.
			list( $year, $month, $day ) = explode( '-', date( 'Y-m-d', strtotime( 'monday next week' ) ) );

			$I->selectDate( 'date', $month, $day, $year );
		}

		$I->seeOptionIsSelected( '#school_period', 'Period 8' );

		$I->see( 'Student S Student' );
	}

	public function setPresent(AcceptanceTester $I)
	{
		$I->save();

		$I->seeElement( '.attendance-code.present' );
	}

	public function administrationSetNA(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/Administration.php' );

		if ( date( 'w' ) > 5 || date( 'w' ) < 1 )
		{
			// Is Saturday or Sunday, select next Monday.
			list( $year, $month, $day ) = explode( '-', date( 'Y-m-d', strtotime( 'monday next week' ) ) );

			$I->selectDate( 'date', $month, $day, $year );
		}

		$I->cantSee( 'Student S Student' );

		$I->selectOption( 'select[name="codes[]"]', 'Present' );

		$I->click( 'Update' );

		$I->waitForAJAX();

		$I->canSee( 'Student S Student' );

		// Fix Add "1" suffix for "Student S Student" ID.
		$I->click( 'div[id^="divattendance1"] .onclick' );

		$I->selectOption( 'div[id^="divattendance1"] select[id$="ATTENDANCE_CODE"]', 'N/A' );

		$I->click( 'Update' );

		$I->waitForAJAX();

		$I->cantSee( 'Student S Student' );
	}

	public function resetPresentForAdmin(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Users/TeacherPrograms.php&include=Attendance/TakeAttendance.php' );

		$I->seeOptionIsSelected( '#period', $this->title_course_period );

		if ( date( 'w' ) > 5 || date( 'w' ) < 1 )
		{
			// Is Saturday or Sunday, select next Monday.
			list( $year, $month, $day ) = explode( '-', date( 'Y-m-d', strtotime( 'monday next week' ) ) );

			$I->selectDate( 'date', $month, $day, $year );
		}

		$I->seeOptionIsSelected( '#school_period', 'Period 8' );

		$I->see( 'Student S Student' );

		$I->save();

		$I->seeElement( '.attendance-code.present' );
	}

	public function dailySummaryCheck(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/DailySummary.php' );

		$I->search( 'Student S Student' );

		list( $year, $month, $day ) = explode( '-', date( 'Y-m-d' ) );

		$I->selectDate( 'start', $month, $day, $year );

		if ( date( 'w' ) > 5 || date( 'w' ) < 1 )
		{
			// Is Saturday or Sunday, select next Monday.
			list( $year, $month, $day ) = explode( '-', date( 'Y-m-d', strtotime( 'monday next week' ) ) );

			$I->selectDate( 'start', $month, $day, $year );

			$I->selectDate( 'end', $month, $day, $year );
		}

		$I->click( 'Go' );

		$I->waitForAJAX();

		// $I->canSee( 'Absent' );
		// Student has 2 courses on AM & PM, he is therefore still present.
		$I->canSee( 'Present' );

		$I->selectOption( '#period_id', 'By Period' );

		$I->waitForAJAX();

		$I->canSee( 'Period 8' );

		$I->seeElement( '.attendance-code.present' );
	}
}
