<?php
/**
 * zCourses - Scheduling tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Scheduling/zCoursesCest.php
 * $ ./test-group-run.sh course
 */

/**
 * @group course
 */
class zCoursesCest
{
	private $title_subject;
	private $title_course;
	private $title_course_period;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function deleteCoursePeriod(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Scheduling/Courses.php' );

		$this->title_subject = '1Test\' ' . date( 'Y-m-d' ) . ' Subject';

		$this->title_course = '1Test\' ' . date( 'Y-m-d' ) . ' Course';

		$this->title_course_period = '1Test\' ' . date( 'Y-m-d' ) . ' CP';

		$I->click( $this->title_subject );

		$I->waitForAJAX();

		$I->click( $this->title_course );

		$I->waitForAJAX();

		// Can't Delete Course having a Course Period.
		$I->cantSee( 'Delete' );

		$I->click( $this->title_course_period );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title_course_period );
	}

	public function deleteCourse(AcceptanceTester $I)
	{
		$I->click( $this->title_subject );

		$I->waitForAJAX();

		// Can't Delete Subject having a Course.
		$I->cantSee( 'Delete' );

		$I->click( $this->title_course );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title_course );
	}

	public function deleteSubject(AcceptanceTester $I)
	{
		$I->click( $this->title_subject );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title_subject );
	}
}
