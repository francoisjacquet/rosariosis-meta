<?php
/**
 * CategoryBreakdown - Discipline tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Discipline/CategoryBreakdownCest.php
 */

class CategoryBreakdownCest
{
	private $option;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function selectDetentionAssigned(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Discipline/CategoryBreakdown.php' );

		$this->option = 'Detention Assigned';

		$I->selectOption( 'select[name="category_id"]', $this->option );

		$I->waitForAJAX();

		$I->seeOptionIsSelected( 'select[name="category_id"]', $this->option );

		$I->seeElement( '#chart0' );
	}

	public function changeStartDate(AcceptanceTester $I)
	{
		$I->selectDate( 'start' );

		$I->click( 'Go' );

		$I->waitForAJAX();

		$I->checkDate( 'start' );
	}

	public function advanced(AcceptanceTester $I)
	{
		$I->click( 'Advanced' );

		$I->waitForAJAX();

		$I->fillField( 'RosarioSIS ID', '1' );

		$I->click( 'Submit' );

		$I->waitForAJAX();

		$I->see( 'RosarioSIS ID: 1' );

		$I->seeOptionIsSelected( 'select[name="category_id"]', $this->option );

		$I->seeElement( '#chart0' );
	}
}
