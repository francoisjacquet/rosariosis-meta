<?php
/**
 * DisciplineForm - Discipline tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Discipline/DisciplineFormCest.php
 */

class DisciplineFormCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Discipline/DisciplineForm.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->selectOption( '#valuesnewDATA_TYPE', 'Checkbox' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Discipline/MakeReferral.php' );

		$I->search( 'Student S Student' );

		$I->checkOption( $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Discipline/DisciplineForm.php' );

		// Second Remove link inside table list.
		$I->click( "/html/body/div[1]/div[2]/form/div[1]/div/table/tbody/tr[1]/td[1]/a[2]/b" );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->cantSee( $this->title );
	}
}
