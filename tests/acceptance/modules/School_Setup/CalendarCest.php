<?php
/**
 * Calendar - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/CalendarCest.php
 */

class CalendarCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Calendar.php&modfunc=create' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#title', $this->title );

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Students/Student.php&include=General_Info&student_id=new' );

		$I->selectOption( 'Calendar', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Calendar.php' );

		$I->selectOption( 'Calendar', $this->title );

		$I->waitForAJAX();

		$I->remove();

		$I->cantSee( $this->title );
	}

	public function setFirstSundayMinutes(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Calendar.php' );

		$this->first_sunday_minutes = 'minutes' .
			date( 'Ymd', strtotime( 'first Sunday of ' . date( 'M' ) . ' ' . date( 'Y' ) ) );

		$I->moveMouseOver( '#' . $this->first_sunday_minutes );

		$I->fillField( '#' . $this->first_sunday_minutes, '150' );

		$I->save();

		$I->moveMouseOver( '#div' . $this->first_sunday_minutes );

		$I->canSee( '150' );
	}

	public function resetFirstSundayMinutes(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Calendar.php' );

		$I->moveMouseOver( '#div' . $this->first_sunday_minutes );

		$I->click( '#div' . $this->first_sunday_minutes . ' .onclick' );

		$I->fillField( '#' . $this->first_sunday_minutes, '' );

		$I->save();

		$I->cantSee( '150' );
	}
}
