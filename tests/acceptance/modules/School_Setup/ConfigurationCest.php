<?php
/**
 * Configuration - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/ConfigurationCest.php
 */

class ConfigurationCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function selectDisplayName(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Configuration.php&tab=system' );

		$I->click( '#divvaluesconfigDISPLAY_NAME .onclick' );

		$I->selectOption( 'Display Name', 'First Name Middle Name Last Name' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->canSee( 'First Name Middle Name Last Name' );
	}

	public function setNumberDaysRotation(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Configuration.php&tab=school' );

		$I->fillField( 'Number of Days for the Rotation', '5' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->click( '#divvaluesschoolsNUMBER_DAYS_ROTATION .onclick' );

		$I->seeInField( '#valuesschoolsNUMBER_DAYS_ROTATION', '5' );

		$I->fillField( 'Number of Days for the Rotation', '' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->cantSeeInField( '#valuesschoolsNUMBER_DAYS_ROTATION', '5' );
	}
}
