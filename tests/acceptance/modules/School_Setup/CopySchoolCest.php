<?php
/**
 * Copy School - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/CopySchoolCest.php
 */

class CopySchoolCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function try(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/CopySchool.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( 'New School\'s Title', $this->title );

		$I->click( 'OK' );

		$I->waitForAJAX();

		$I->see( $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Schools.php' );

		// Change school first.
		$I->selectOption( '#school', $this->title );

		$I->waitForAJAX();

		$I->see( $this->title );

		$I->delete();

		$I->cantSee( $this->title );

		// Make sure we are in default school.
		$I->selectOption( '#school', 'Default School' );
	}
}
