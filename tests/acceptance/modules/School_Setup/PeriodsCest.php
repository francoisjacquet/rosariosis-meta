<?php
/**
 * Periods - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/PeriodsCest.php
 */

class PeriodsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Periods.php' );

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test1' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Scheduling/MassRequests.php' );

		$I->search( 'Student S Student' );

		$I->selectOption( 'select[name=with_period_id]', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Periods.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
