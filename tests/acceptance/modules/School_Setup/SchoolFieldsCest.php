<?php
/**
 * SchoolFields - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/SchoolFieldsCest.php
 */

class SchoolFieldsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/SchoolFields.php' );

		$I->click( 'Add' );

		$I->waitForAJAX();

		$this->title = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#ML_tablesnewTITLEen_USutf8', $this->title );

		$I->fillField( '#tablesnewSELECT_OPTIONS', "TestOption1\nTestOption2\nTestOption3" );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Schools.php' );

		$I->selectOption( $this->title, 'TestOption1' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->canSee( 'TestOption1' );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/SchoolFields.php' );

		$I->click( $this->title );

		$I->waitForAJAX();

		$I->delete();

		$I->cantSee( $this->title );
	}
}
