<?php
/**
 * Schools - School_Setup tests
 *
 * $ ./test-run.sh tests/acceptance/modules/School_Setup/SchoolsCest.php
 */

class SchoolsCest
{
	private $title;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function save(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=School_Setup/Schools.php' );

		$I->fillField( 'School Number', '123456' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->click( '#divvaluesSCHOOL_NUMBER .onclick' );

		$I->seeInField( '#valuesSCHOOL_NUMBER', '123456' );

		$I->fillField( 'School Number', '' );

		// Fix error Save button not on screen.
		$I->scrollTop();

		$I->save();

		$I->cantSeeInField( '#valuesSCHOOL_NUMBER', '123456' );
	}
}
