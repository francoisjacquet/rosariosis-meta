<?php
/**
 * AttendanceCodes - Attendance tests
 *
 * $ ./test-run.sh tests/acceptance/modules/Attendance/AttendanceCodesCest.php
 */

class AttendanceCodesCest
{
	private $title,
		$code;

	public function _before(AcceptanceTester $I)
	{
		$I->login( 'admin' );
	}

	// Tests.
	public function add(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/AttendanceCodes.php' );

		$this->title = 'Test ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->title );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test1' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->title );
	}

	public function checkAdd(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/AddAbsences.php' );

		$I->search( 'Student S Student' );

		$I->selectOption( 'Absence Code', $this->title );
	}

	public function delete(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/AttendanceCodes.php' );

		$I->remove();

		$I->cantSee( $this->title );
	}

	public function addCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/AttendanceCodes.php&table=new' );

		$this->title = 'TestCat\'';

		$I->fillField( '#valuesnewTITLE', $this->title );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->click( $this->title );

		$I->waitForAJAX();

		$this->code = 'Test\' ' . date( 'Y-m-d H:i:s' );

		$I->fillField( '#valuesnewTITLE', $this->code );

		$I->fillField( '#valuesnewSHORT_NAME', 'Test2' );

		// Set Sort Order to -1 so is first of list (then easier to remove).
		$I->fillField( '#valuesnewSORT_ORDER', '-1' );

		$I->save();

		$I->canSee( $this->code );
	}

	public function checkAddCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/Administration.php' );

		$I->click( $this->title );

		$I->waitForAJAX();

		if ( $this->code )
		{
			$I->selectOption( '#code_pulldowns select', $this->code );
		}
	}

	public function deleteCategory(AcceptanceTester $I)
	{
		$I->amOnPage( '/Modules.php?modname=Attendance/AttendanceCodes.php&table=new' );

		$I->remove();

		$I->cantSee( $this->title );
	}
}
