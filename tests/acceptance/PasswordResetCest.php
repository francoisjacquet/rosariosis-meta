<?php
/**
 * Password Reset tests
 *
 * $ ./test-run.sh tests/acceptance/PasswordResetCest.php
 */

// @todo Actual Password Reset.
class PasswordResetCest
{
	public function _before(AcceptanceTester $I)
	{
	}

	// Tests.
	public function clickPasswordHelp(AcceptanceTester $I)
	{
		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		$I->click( 'Password help' );

		$I->seeInCurrentUrl( 'PasswordReset.php' );
	}

	public function submitFormIncorrectEmail(AcceptanceTester $I)
	{
		require_once '../config.inc.php';

		$I->fillField( 'Email', 'nonexisting@nonexistingdomain.com' );

		$I->click( 'Send password reset instructions' );

		$I->expect( 'Reset instructions not sent for incorrect email.' );

		if ( defined( 'ROSARIO_DEBUG' ) && ROSARIO_DEBUG )
		{
			$I->seeInCurrentUrl( 'PasswordReset.php' );

			$I->see( 'No account with this email were found.', '.error' );
		}
		else
		{
			$I->seeInCurrentUrl( 'index.php' );
		}
	}

	public function submitFormCorrectEmail(AcceptanceTester $I)
	{
		require_once '../config.inc.php';

		$I->fillField( 'Email', 'admin@admin.com' );

		$I->click( 'Send password reset instructions' );

		$I->expect( 'Reset instructions sent for correct email.' );

		if ( defined( 'ROSARIO_DEBUG' ) && ROSARIO_DEBUG )
		{
			$I->seeInCurrentUrl( 'PasswordReset.php' );

			// $I->see( 'Success.', '.updated' );
		}
		else
		{
			$I->seeInCurrentUrl( 'index.php' );
		}
	}
}
