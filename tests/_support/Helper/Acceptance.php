<?php
namespace Helper;

use Facebook\WebDriver\WebDriverBy;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
	// @link https://stackoverflow.com/questions/26183792/use-codeception-assertion-in-conditional-if-statement
	function seePageHasElement( $element )
	{
		$nodes = $this->getModule( 'WebDriver' )->_findElements( $element );

		return ! empty( $nodes );
	}

	function seePageHasLink( $text )
	{
		$nodes = $this->getModule( 'WebDriver' )->_findElements( WebDriverBy::partialLinkText( $text ) );

		return ! empty( $nodes );
	}
}
