<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    private $am_logged_in_as = '';

   /**
    * Define custom actions here
    */
	public function login( $username, $password = '' )
	{
		$I = $this;

		if ( $I->am_logged_in_as
			&& $I->am_logged_in_as !== $username )
		{
			$I->logout();
		}

		// If snapshot exists - skip login.
		if ( $I->am_logged_in_as === $username || $I->loadSessionSnapshot( 'login_' . $username ) )
		{
			return;
		}

		$I->amOnPage( '/index.php?locale=en_US.utf8' );

		// Logging in.
		$I->submitForm( '#loginform',
			array(
				'USERNAME' => $username,
				'PASSWORD' => ( $password ? $password : $username ),
			)
		);

		$I->am_logged_in_as = $username;

		// Saving snapshot.
		$I->saveSessionSnapshot( 'login_' . $username );
	}

	public function logout()
	{
		$I = $this;

		$I->waitForAJAX();

		$I->amOnPage( '/index.php?modfunc=logout' );

		// Delete snapshot.
		$I->deleteSessionSnapshot( 'login_' . $I->am_logged_in_as );

		$I->am_logged_in_as = '';
	}

	public function waitForAJAX()
	{
		$I = $this;

		$I->waitForJS( 'return $.active == 0;', 4 );
	}

	public function save()
	{
		$I = $this;

		$I->scrollTop();

		$I->click( 'Save' );

		$I->waitForAJAX();
	}

	public function delete()
	{
		$I = $this;

		$I->click( 'Delete' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();
	}

	public function remove()
	{
		$I = $this;

		$I->click( 'Remove' );

		$I->waitForAJAX();

		$I->click( 'OK' );

		$I->waitForAJAX();
	}

	public function scrollTop()
	{
		$I = $this;

		$I->executeJS( 'document.body.scrollIntoView();' );
	}

	public function scrollBottom( $height = 0 )
	{
		$I = $this;

		if ( ! $height )
		{
			$height = '$(document).height()';
		}

		$I->executeJS( '$(document).scrollTop(' . $height . ');' );
	}

	public function selectDate( $name, $month = '', $day = '', $year = '' )
	{
		$I = $this;

		if ( ! $month )
		{
			$month = date( 'm' );
		}

		if ( ! $day )
		{
			$day = date( 'd' );
		}

		if ( ! $year )
		{
			$year = date( 'Y' );
		}

		$I->selectOption( 'select[name="month_' . $name . '"]', $month );

		$I->waitForAJAX();

		$I->selectOption( 'select[name="day_' . $name . '"]', (int) $day );

		$I->waitForAJAX();

		$I->selectOption( 'select[name="year_' . $name . '"]', $year );

		$I->waitForAJAX();
	}

	public function checkDate( $name, $month = '', $day = '', $year = '' )
	{
		$I = $this;

		if ( ! $month )
		{
			$month = date( 'M' );
		}

		if ( ! $day )
		{
			$day = date( 'd' );
		}

		if ( ! $year )
		{
			$year = date( 'Y' );
		}

		$I->seeOptionIsSelected( 'select[name="month_' . $name . '"]', $month );

		$I->seeOptionIsSelected( 'select[name="day_' . $name . '"]', (int) $day );

		$I->seeOptionIsSelected( 'select[name="year_' . $name . '"]', $year );
	}

	public function search( $name, $person_type = 'student' )
	{
		$I = $this;

		if ( $name && $I->seePageHasElement( '.current-person.' . $person_type ) )
		{
			// The user/student is already selected.
			return;
		}

		if ( $name && $person_type === 'self'
			&& $I->seePageHasElement( '.current-person.staff' ) )
		{
			// A user is already selected, clear.
			$I->click( 'Clear working user' );
		}

		$I->click( 'Submit' );

		$I->waitForAJAX();

		if ( $name && $I->seePageHasElement( '.current-person.' . $person_type ) )
		{
			// The user/student is already selected.
			return;
		}

		if ( $name && $I->seePageHasLink( $name ) )
		{
			$I->click( $name );

			$I->waitForAJAX();
		}
	}
}
