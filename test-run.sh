#!/bin/bash

echo "Test run: $1"

# Empty previous tests output.
rm public/tests/_output/*

chromedriver --url-base=/wd/hub --port=9515 &

php codecept.phar run $1 --steps --debug --html

pkill chromedriver
