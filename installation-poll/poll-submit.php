<?php
/**
 * Installation Poll for RosarioSIS
 */

require_once 'config.inc.php';
require_once 'functions.inc.php';

if ( empty( $_POST ) )
{
	if ( ! empty( $_SERVER['HTTP_USER_AGENT'] ) )
	{
		header( 'Location: index.php' );
	}

	// Ping from new install.
	die( '1' );
}
else
{
	// Fix XHR blocked CORS Missing Allow Origin
	header( 'Access-Control-Allow-Origin: *' );

	if ( ( empty( $_POST['locale'] )
			|| strlen( $_POST['locale'] ) !== 10 )
		|| ( empty( $_POST['version'] )
			|| version_compare( $_POST['version'], '4.6-beta', '<' ) ) )
	{
		// Invalid locale or version.
		die( 'Invalid locale or version.' );
	}

	$ip = ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )
		// Filter IP, HTTP_* headers can be forged.
		&& filter_var( $_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP ) ?
		$_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] );

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$poll_data = array();

	$poll_data['IP'] = $ip;

	$poll_data['USER_AGENT'] = $user_agent;

	$poll_data['CREATED_AT'] = date( 'Y-m-d H:i:s' );

	$poll_data['LOCALE'] = $_POST['locale'];

	$poll_data['VERSION'] = $_POST['version'];

	$poll_data['DATABASE'] = empty( $_POST['database'] ) ? '' : $_POST['database'];

	$poll_data['DATABASE_VERSION'] = empty( $_POST['database_version'] ) ? '' : $_POST['database_version'];

	$poll_data['PHP_VERSION'] = empty( $_POST['php_version'] ) ? '' : $_POST['php_version'];

	$poll_data['USAGE'] = empty( $_POST['usage'] ) ? '' : $_POST['usage'];

	$poll_data['SCHOOL'] = empty( $_POST['school'] ) ? '' : $_POST['school'];

	$poll_data['ORGANIZATION'] = empty( $_POST['organization'] ) ? '' : $_POST['organization'];

	$poll_data['STUDENTS'] = empty( $_POST['students'] ) ? '' : $_POST['students'];

	// Save info to DB.
	$saved = InstallationPollSave( $poll_data );

	die( 'Installation poll' . ( $saved ? ' ' : ' not ' ) . 'saved!' );
}
