<?php
/**
 * Installation poll functions
 */

function PollAnswers( $lang )
{
	$file = 'index.html';

	if ( $lang !== 'en' )
	{
		$file = '../' . $lang . '/installation-poll/index.html';

		$locale = $lang === 'es' ? 'es_ES.utf8' : 'fr_FR.utf8';

		setlocale( LC_ALL, $locale );
	}

	// Get index.html.
	$index_html = file_get_contents( $file );

	$poll_answers = GetPollAnswers( $lang );

	echo str_replace( '<p>[POLL_ANSWERS]</p>', $poll_answers, $index_html );
}

function GetPollAnswers( $lang )
{
	$new_file = false;

	// Cache file: Last modified date.
	if ( ! file_exists( 'poll_answers_' . $lang . '.txt' ) )
	{
		// Create file.
		$file = fopen( 'poll_answers_' . $lang . '.txt', 'w' ) or die( 'can\'t open file' );

		fclose( $file );

		$new_file = true;
	}

	// Cache file: Last modified date.
	if ( $new_file
		|| (int) filemtime( 'poll_answers_' . $lang . '.txt' ) < ( time() - 86400 ) )
	{
		// Get new entries country via ipstack API.
		PollEntriesCountry();

		// Reset cache every day.
		$poll_entries = GetPollEntries( $lang );

		$poll_answers = array();

		foreach ( $poll_entries as $title => $content )
		{
			// var_dump( $content );

			$title = '<h2>' . $title . '</h2>';

			$content = '<table style="width: auto"><tr><td>' .
				implode( '</td></tr><tr><td>', $content ) .
				'</td></tr></table>';

			$entry_post = $title . $content;

			$poll_answers[] = $entry_post;
		}

		if ( $poll_answers )
		{
			$poll_answers = implode( '', $poll_answers );
		}
		else
		{
			$poll_answers = 'No posts were found.';
		}

		// Save in file.
		file_put_contents( 'poll_answers_' . $lang . '.txt', $poll_answers );
	}
	else
	{
		$poll_answers = file_get_contents( 'poll_answers_' . $lang . '.txt' );
	}

	return $poll_answers;
}

function GetPollEntries( $lang )
{
	$poll_entries = array();

	$poll_entries += GetEntryAnswers( $lang );

	$poll_entries += GetEntryLocales( $lang );

	$poll_entries += GetEntryCountries( $lang );

	$poll_entries += GetEntryUsage( $lang );

	$poll_entries += GetEntrySchool( $lang );

	$poll_entries += GetEntryOrganization( $lang );

	$poll_entries += GetEntryStudents( $lang );

	$poll_entries += GetEntryVersions( $lang );

	$poll_entries += GetEntryPHP( $lang );

	$poll_entries += GetEntryDatabaseMySQL( $lang );

	$poll_entries += GetEntryDatabasePostgreSQL( $lang );

	return $poll_entries;
}

function GetAnswersTotal( $not_null_column = '' )
{
	$where_sql = $not_null_column ?
		" WHERE " . DBEscapeIdentifier( $not_null_column ) . " IS NOT NULL" : '';

	$answers_total = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL" . $where_sql );

	return $answers_total;
}


function GetEntryAnswers( $lang )
{
	$answers_total = GetAnswersTotal();

	$answers_per_month = DBGetOne( "SELECT AVG(ip.PER_MONTH)
		FROM (SELECT COUNT(1) as PER_MONTH
      		FROM INSTALLATION_POLL GROUP BY DATE_TRUNC('month', CREATED_AT)) ip" );

	$answers_label = array(
		'en' => 'Answers',
		'es' => 'Respuestas',
		'fr' => 'Réponses',
	);

	$total_label = array(
		'en' => 'Total since %s</td><td>%d',
		'es' => 'Total desde %s</td><td>%d',
		'fr' => 'Total depuis %s</td><td>%d',
	);

	$month_label = array(
		'en' => 'Per month</td><td>%d',
		'es' => 'Por mes</td><td>%d',
		'fr' => 'Par mois</td><td>%d',
	);

	return array(
		$answers_label[ $lang ] => array(
		sprintf( $total_label[ $lang ], ucfirst( strftime( '%B %e %Y', strtotime( '2019-05-01' ) ) ), $answers_total ),
		sprintf( $month_label[ $lang ], $answers_per_month ),
	) );
}


function GetEntryLocales( $lang )
{
	$answers_total = GetAnswersTotal();

	$locales = DBGet( "SELECT LOWER(LOCALE) AS LOCALE,COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		GROUP BY LOWER(LOCALE)
		ORDER BY TOTAL DESC
		LIMIT 10" );

	$locales_label = array(
		'en' => 'Language',
		'es' => 'Idioma',
		'fr' => 'Langue',
	);

	$languges = array();

	foreach ( $locales as $locale )
	{
		$language = ucfirst( locale_get_display_language( $locale['LOCALE'], $lang ) );

		// Requires php-intl extension.
		if ( ! isset( $languages[ $language ] ) )
		{
			$languages[ $language ] = 0;
		}

		$languages[ $language ] += $locale['TOTAL'];
	}

	$poll_entry = array( $locales_label[ $lang ] => array() );

	foreach ( $languages as $language => $language_total )
	{
		if ( ! MinPollPercent( $language_total, $answers_total ) )
		{
			continue;
		}

		$poll_entry[ $locales_label[ $lang ] ][] = MakePollPercent( $language, $language_total, $answers_total );
	}

	return $poll_entry;
}

function GetEntryUsage( $lang )
{
	$answers_total = GetAnswersTotal( 'USAGE' );

	$usages = DBGet( "SELECT USAGE,COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE USAGE IS NOT NULL
		GROUP BY USAGE
		ORDER BY TOTAL DESC" );

	$usage_options = array(
		'en' => array(
			'testing' => 'Testing',
			'production' => 'Production',
		),
		'es' => array(
			'testing' => 'Prueba',
			'production' => 'Producción',
		),
		'fr' => array(
			'testing' => 'Test',
			'production' => 'Production',
		),
	);

	$usage_label = array(
		'en' => 'Usage',
		'es' => 'Uso',
		'fr' => 'Usage',
	);

	$poll_entry = array( $usage_label[ $lang ] => array() );

	foreach ( $usages as $usage )
	{
		$usage_option = $usage_options[ $lang ][ $usage['USAGE'] ];

		$poll_entry[ $usage_label[ $lang ] ][] = MakePollPercent( $usage_option, $usage['TOTAL'], $answers_total );
	}

	return $poll_entry;
}

function GetEntrySchool( $lang )
{
	$answers_total = GetAnswersTotal( 'SCHOOL' );

	$schools = DBGet( "SELECT SCHOOL,COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE SCHOOL IS NOT NULL
		GROUP BY SCHOOL
		ORDER BY TOTAL DESC" );

	$school_options = array(
		'en' => array(
			'primary' => 'Primary',
			'secondary' => 'Secondary',
			'superior' => 'Superior',
			'other' => 'Other',
		),
		'es' => array(
			'primary' => 'Primario',
			'secondary' => 'Secundario',
			'superior' => 'Superior',
			'other' => 'Otro',
		),
		'fr' => array(
			'primary' => 'Primaire',
			'secondary' => 'Secondaire',
			'superior' => 'Supérieur',
			'other' => 'Autre',
		),
	);

	$school_label = array(
		'en' => 'School',
		'es' => 'Escuela',
		'fr' => 'École',
	);

	$poll_entry = array( $school_label[ $lang ] => array() );

	foreach ( $schools as $school )
	{
		$school_option = $school_options[ $lang ][ $school['SCHOOL'] ];

		$poll_entry[ $school_label[ $lang ] ][] = MakePollPercent( $school_option, $school['TOTAL'], $answers_total );
	}

	return $poll_entry;
}

function GetEntryOrganization( $lang )
{
	$answers_total = GetAnswersTotal( 'ORGANIZATION' );

	$organizations = DBGet( "SELECT ORGANIZATION,COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE ORGANIZATION IS NOT NULL
		GROUP BY ORGANIZATION
		ORDER BY TOTAL DESC" );

	$organization_options = array(
		'en' => array(
			'private' => 'Private',
			'public' => 'Public',
			'non-profit' => 'Non-profit',
		),
		'es' => array(
			'private' => 'Privada',
			'public' => 'Publica',
			'non-profit' => 'Sin ánimo de lucro',
		),
		'fr' => array(
			'private' => 'Privée',
			'public' => 'Publique',
			'non-profit' => 'À but non lucratif',
		),
	);

	$organization_label = array(
		'en' => 'Organization',
		'es' => 'Organización',
		'fr' => 'Organisation',
	);

	$poll_entry = array( $organization_label[ $lang ] => array() );

	foreach ( $organizations as $organization )
	{
		$organization_option = $organization_options[ $lang ][ $organization['ORGANIZATION'] ];

		$poll_entry[ $organization_label[ $lang ] ][] = MakePollPercent( $organization_option, $organization['TOTAL'], $answers_total );
	}

	return $poll_entry;
}

function GetEntryStudents( $lang )
{
	$answers_total = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL WHERE STUDENTS>9 AND STUDENTS<99999" );

	$students = array();

	$students[] = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE STUDENTS IS NOT NULL AND STUDENTS>9 AND STUDENTS<=100" );

	$students[] = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE STUDENTS IS NOT NULL AND STUDENTS<=500 AND STUDENTS>100" );

	$students[] = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE STUDENTS IS NOT NULL AND STUDENTS<=2000 AND STUDENTS>500" );

	$students[] = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		WHERE STUDENTS IS NOT NULL AND STUDENTS>2000 AND STUDENTS<99999" );

	$student_options = array(
		'&le;100',
		'&le;500',
		'&le;2000',
		'&gt;2000',
	);

	$students_label = array(
		'en' => 'Students',
		'es' => 'Estudiantes',
		'fr' => 'Élèves',
	);

	$poll_entry = array( $students_label[ $lang ] => array() );

	foreach ( $students as $i => $student )
	{
		$student_option = $student_options[ $i ];

		$poll_entry[ $students_label[ $lang ] ][] = MakePollPercent( $student_option, $student, $answers_total );
	}

	return $poll_entry;
}

function GetEntryCountries( $lang )
{
	$answers_total = DBGetOne( "SELECT COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL WHERE COUNTRY IS NOT NULL AND COUNTRY<>''" );

	$countries = DBGet( "SELECT COUNT(1) AS TOTAL,COUNTRY
		FROM INSTALLATION_POLL
		GROUP BY COUNTRY
		ORDER BY TOTAL DESC,COUNTRY
		LIMIT 10" );

	$countries_label = array(
		'en' => 'Countries',
		'es' => 'Paises',
		'fr' => 'Pays',
	);

	$poll_entry = array( $countries_label[ $lang ] => array() );

	$distinct_countries = DBGetOne( "SELECT COUNT(DISTINCT COUNTRY)
		FROM INSTALLATION_POLL" );

	$poll_entry[ $countries_label[ $lang ] ][] = $countries_label[ $lang ] . '</td><td>' . $distinct_countries;

	foreach ( $countries as $country )
	{
		$country_name = $country['COUNTRY'];

		$country_total = $country['TOTAL'];

		$poll_entry[ $countries_label[ $lang ] ][] = MakePollPercent( $country_name, $country_total, $answers_total );
	}

	return $poll_entry;
}


function GetEntryVersions( $lang )
{
	$answers_total = GetAnswersTotal();

	$versions = DBGet( "SELECT SUBSTRING(VERSION, 1, POSITION('.' IN VERSION) -1) AS VER,COUNT(1) AS TOTAL
		FROM INSTALLATION_POLL
		GROUP BY SUBSTRING(VERSION, 1, POSITION('.' IN VERSION) -1)
		ORDER BY TOTAL DESC" );

	$versions_label = array(
		'en' => 'RosarioSIS versions',
		'es' => 'Versiones de RosarioSIS',
		'fr' => 'Versions de RosarioSIS',
	);

	$poll_entry = array( $versions_label[ $lang ] => array() );

	foreach ( $versions as $version )
	{
		$poll_entry[ $versions_label[ $lang ] ][] = MakePollPercent( $version['VER'], $version['TOTAL'], $answers_total );
	}

	return $poll_entry;
}


function GetEntryPHP( $lang )
{
	$answers_total = GetAnswersTotal( 'PHP_VERSION' );

	$versions = DBGet( "SELECT PHP_VERSION AS VER,COUNT(1) AS TOTAL
	FROM INSTALLATION_POLL
	WHERE PHP_VERSION IS NOT NULL
	GROUP BY VER
	ORDER BY TOTAL DESC,VER ASC" );

	$versions_label = array(
		'en' => 'PHP versions',
		'es' => 'Versiones de PHP',
		'fr' => 'Versions de PHP',
	);

	$poll_entry = array( $versions_label[ $lang ] => array() );

	foreach ( $versions as $version )
	{
		if ( ! MinPollPercent( $version['TOTAL'], $answers_total ) )
		{
			continue;
		}

		$poll_entry[ $versions_label[ $lang ] ][] = MakePollPercent( $version['VER'], $version['TOTAL'], $answers_total );
	}

	return $poll_entry;
}


function GetEntryDatabasePostgreSQL( $lang )
{
	$answers_total = GetAnswersTotal( 'DATABASE' );

	$postgresql_total = DBGetOne( "SELECT COUNT(1)
		FROM INSTALLATION_POLL
		WHERE DATABASE='postgresql'" );

	$versions = DBGet( "SELECT CASE POSITION('.' IN DATABASE_VERSION)
		WHEN 0 THEN DATABASE_VERSION
		ELSE SUBSTRING(DATABASE_VERSION, 1, POSITION('.' IN DATABASE_VERSION) -1)
	END AS VER,COUNT(1) AS TOTAL
	FROM INSTALLATION_POLL
	WHERE DATABASE='postgresql'
	GROUP BY VER
	ORDER BY TOTAL DESC,VER ASC" );

	$versions_label = array(
		'en' => 'PostgreSQL versions <small>%s</small>',
		'es' => 'Versiones de PostgreSQL <small>%s</small>',
		'fr' => 'Versions de PostgreSQL <small>%s</small>',
	);

	$label = sprintf(
		$versions_label[ $lang ],
		MakePollPercent( '', $postgresql_total, $answers_total )
	);

	$poll_entry = array( $label => array() );

	foreach ( $versions as $version )
	{
		if ( ! MinPollPercent( $version['TOTAL'], $answers_total ) )
		{
			continue;
		}

		$poll_entry[ $label ][] = MakePollPercent( $version['VER'], $version['TOTAL'], $answers_total );
	}

	return $poll_entry;
}


function GetEntryDatabaseMySQL( $lang )
{
	$answers_total = GetAnswersTotal( 'DATABASE' );

	$mysql_total = DBGetOne( "SELECT COUNT(1)
		FROM INSTALLATION_POLL
		WHERE DATABASE='mysql'" );

	$versions = DBGet( "SELECT DATABASE_VERSION AS VER,COUNT(1) AS TOTAL
	FROM INSTALLATION_POLL
	WHERE DATABASE='mysql'
	GROUP BY VER
	ORDER BY TOTAL DESC,VER ASC" );

	$versions_label = array(
		'en' => 'MySQL versions <small>%s</small>',
		'es' => 'Versiones de MySQL <small>%s</small>',
		'fr' => 'Versions de MySQL <small>%s</small>',
	);

	$label = sprintf(
		$versions_label[ $lang ],
		MakePollPercent( '', $mysql_total, $answers_total )
	);

	$poll_entry = array( $label => array() );

	foreach ( $versions as $version )
	{
		if ( ! MinPollPercent( $version['TOTAL'], $answers_total ) )
		{
			continue;
		}

		$poll_entry[ $label ][] = MakePollPercent( $version['VER'], $version['TOTAL'], $answers_total );
	}

	return $poll_entry;
}


function MakePollPercent( $label, $value, $total )
{
	return sprintf(
		( $label ? '%s</td><td>%s' : '%s%s' ),
		$label,
		round( @( $value / $total ) * 100, 0 ) . '%'
	);
}

function MinPollPercent( $value, $total, $min_percent = 0.33 )
{
	return ( @( $value / $total ) * 100 ) >= $min_percent;
}


/*
CREATE TABLE installation_poll (
    ip character varying(50) NOT NULL,
    user_agent text,
    created_at timestamp(0) without time zone NOT NULL,
    locale character varying(10),
    version character varying(20),
    usage character varying(20),
    school character varying(20),
    students numeric(6,0),
    organization character varying(20),
    country character varying(50),
    database character varying(20),
    database_version character varying(20),
    php_version character varying(20)
);
 */

function InstallationPollSave( $poll_data )
{
	$exists_sql = "SELECT 1 FROM INSTALLATION_POLL
		WHERE IP='" . DBEscapeString( $poll_data['IP'] ) . "'
		AND USER_AGENT='" . DBEscapeString( $poll_data['USER_AGENT'] ) . "'
		AND CREATED_AT>=(CURRENT_TIMESTAMP - INTERVAL '1 day')";

	if ( DBGetOne( $exists_sql ) )
	{
		return false;
	}

	$insert_sql = "INSERT INTO INSTALLATION_POLL (IP,USER_AGENT,CREATED_AT,LOCALE,VERSION,USAGE,
		DATABASE,DATABASE_VERSION,PHP_VERSION,SCHOOL,ORGANIZATION,STUDENTS)
	VALUES('" . DBEscapeString( $poll_data['IP'] ) . "','" . DBEscapeString( $poll_data['USER_AGENT'] ) . "','" .
	DBEscapeString( $poll_data['CREATED_AT'] ) . "','" . DBEscapeString( $poll_data['LOCALE'] ) . "','" .
	DBEscapeString( $poll_data['VERSION'] ) . "','" . DBEscapeString( $poll_data['USAGE'] ) . "','" .
	DBEscapeString( $poll_data['DATABASE'] ) . "','" . DBEscapeString( $poll_data['DATABASE_VERSION'] ) . "','" .
	DBEscapeString( $poll_data['PHP_VERSION'] ) . "','" .
	DBEscapeString( $poll_data['SCHOOL'] ) . "','" . DBEscapeString( $poll_data['ORGANIZATION'] ) . "','" .
	(int) $poll_data['STUDENTS'] . "')";

	return DBQuery( $insert_sql );
}


/**
 * Establish DB connection
 *
 * @global $DatabaseServer   Database server hostname
 * @global $DatabaseUsername Database username
 * @global $DatabasePassword Database password
 * @global $DatabaseName     Database name
 * @global $DatabasePort     Database port
 * @see config.inc.php file for globals definitions
 *
 * @return PostgreSQL connection resource
 */
function db_start()
{
	global $DatabaseServer,
		$DatabaseUsername,
		$DatabasePassword,
		$DatabaseName,
		$DatabasePort;

	/**
	 * Fix pg_connect(): Unable to connect to PostgreSQL server:
	 * could not connect to server:
	 * No such file or directory Is the server running locally
	 * and accepting connections on Unix domain socket "/tmp/.s.PGSQL.5432"
	 *
	 * Always set host, force TCP.
	 *
	 * @since 3.5.2
	 */
	$connectstring = 'host=' . $DatabaseServer . ' ';

	if ( $DatabasePort !== '5432' )
	{
		$connectstring .= 'port=' . $DatabasePort . ' ';
	}

	$connectstring .= 'dbname=' . $DatabaseName . ' user=' . $DatabaseUsername;

	if ( $DatabasePassword !== '' )
	{
		$connectstring .= ' password=' . $DatabasePassword;
	}

	$connection = pg_connect( $connectstring );

	// Error code for both.

	if ( $connection === false )
	{
		// TRANSLATION: do NOT translate these since error messages need to stay in English for technical support.
		error_log(
			sprintf( "Could not Connect to Database Server '%s'.", $DatabaseServer ) . ' ' . pg_last_error()
		);
	}

	return $connection;
}


/**
 * This function connects, and does the passed query, then returns a result resource
 * Not receiving the return == unusable search.
 *
 * @example $processable_results = DBQuery( "SELECT * FROM students" );
 *
 * @since 3.7 INSERT INTO case to Replace empty strings ('') with NULL values.
 * @since 4.3 Performance: static DB $connection.
 * @since 4.3 Do DBQuery after action hook.
 *
 * @param  string   $sql       SQL statement.
 * @return resource PostgreSQL result resource
 */
function DBQuery( $sql )
{
	static $connection;

	if ( ! isset( $connection ) )
	{
		$connection = db_start();
	}

	// Replace empty strings ('') with NULL values.

	if ( stripos( $sql, 'INSERT INTO ' ) !== false )
	{
		// Check for ( or , character before empty string ''.
		$sql = preg_replace( "/([,\(])[\r\n\t ]*''(?!')/", '\\1NULL', $sql );
	}

	// Check for <> or = character before empty string ''.
	$sql = preg_replace( "/(<>|=)[\r\n\t ]*''(?!')/", '\\1NULL', $sql );

	/**
	 * IS NOT NULL cases
	 *
	 * Replace <>NULL & !=NULL with IS NOT NULL
	 *
	 * @link http://www.postgresql.org/docs/current/static/functions-comparison.html
	 */
	$sql = str_replace(
		array( '<>NULL', '!=NULL' ),
		array( ' IS NOT NULL', ' IS NOT NULL' ),
		$sql
	);

	$result = @pg_exec( $connection, $sql );

	return $result;
}


/**
 * DB Get One
 * Get one (first) column (& row) value from database.
 *
 * @since 4.5
 *
 * @example $school_year = DBGetOne( "SELECT SYEAR FROM TABLE WHERE SYEAR='2019'" );
 *
 * @param string $sql_select SQL SELECT query.
 *
 * @return string False or DB value.
 */
function DBGetOne( $sql_select )
{
	$QI = DBQuery( $sql_select );

	$RET = (array) db_fetch_row( $QI );

	return reset( $RET );
}


/**
 * DB Get
 * Get one (first) row from database.
 *
 * @since 4.5
 *
 * @example $school_year = DBGet( "SELECT SYEAR FROM TABLE WHERE SYEAR='2019'" );
 *
 * @param string $sql_select SQL SELECT query.
 *
 * @return string False or DB value.
 */
function DBGet( $sql_select )
{
	$QI = DBQuery( $sql_select );

	$RET = array();

	while ( $row = db_fetch_row( $QI ) )
	{
		$RET[] = $row;
	}

	return $RET;
}


/**
 * Return next row
 *
 * @param  resource PostgreSQL result resource $result Result.
 * @return array    	Next row in result set.
 */
function db_fetch_row( $result )
{
	$return = @pg_fetch_array( $result, null, PGSQL_ASSOC );

	return is_array( $return ) ? @array_change_key_case( $return, CASE_UPPER ) : $return;
}


/**
 * Escapes single quotes by using two for every one.
 *
 * @example $safe_string = DBEscapeString( $string );
 *
 * @param  string $input  Input string.
 * @return string escaped string
 */
function DBEscapeString( $input )
{
	// return str_replace("'","''",$input);
	return pg_escape_string( $input );
}

/**
 * Escapes identifiers (table, column) using double quotes.
 * Security function for
 * when you HAVE to use a variable as an identifier.
 *
 * @example $safe_sql = "SELECT COLUMN FROM " . DBEscapeIdentifier( $table ) . " WHERE " . DBEscapeIdentifier( $column ) . "='Y'";
 * @uses pg_escape_identifier(), requires PHP 5.4.4+
 * @since 3.0
 *
 * @param  string $identifier SQL identifier (table, column).
 * @return string Escaped identifier.
 */
function DBEscapeIdentifier( $identifier )
{
	$identifier = mb_strtolower( $identifier );

	if ( ! function_exists( 'pg_escape_identifier' ) )
	{
		return '"' . $identifier . '"';
	}

	return pg_escape_identifier( $identifier );
}


/**
 * Set Country for Poll entries based on IP address
 *
 * @uses IPtoCountry()
 */
function PollEntriesCountry()
{
	$no_country_ip = DBGet( "SELECT DISTINCT IP
		FROM INSTALLATION_POLL
		WHERE COUNTRY IS NULL;" );

	$ip_countries = array();

	foreach ( $no_country_ip as $ip )
	{
		$ip_real = $ip['IP'];

		$country = IPtoCountry( $ip_real );

		if ( ! $country )
		{
			$country = 'null';
		}

		if ( strlen( $country ) > 50 )
		{
			$country = substr( $country, 0, 50 );
		}

		$ip_countries[ $country ][] = $ip_real;
	}

	foreach ( $ip_countries as $country => $ips )
	{
		if ( $country === 'null' )
		{
			// Set empty so we do not query it again.
			$country = '';
		}

		$update_sql = "UPDATE INSTALLATION_POLL
			SET COUNTRY='" . DBEscapeString( $country ) . "'
			WHERE COUNTRY IS NULL
			AND IP IN('" . implode( "','", $ips ) . "')";

		DBQuery( $update_sql );
	}
}


/**
 * IP to country
 * Uses ipgeolocation.io API, 30000 requests per month, 1000/day free.
 * @link https://ipgeolocation.io/ip-location-api.html#get-specific-fields-only
 *
 * @param string $ip IP address.
 *
 * @return string
 */
function IPtoCountry( $ip )
{
	global $ipgeolocationAPIKey;

	if ( ! $ip
		|| ! $ipgeolocationAPIKey )
	{
		return '';
	}

	$api_url = 'https://api.ipgeolocation.io/ipgeo?apiKey=' . $ipgeolocationAPIKey . '&ip=' . $ip . '&fields=country_name';

	$response = file_get_contents( $api_url );

	$json = json_decode( $response, true );

	if ( json_last_error() !== JSON_ERROR_NONE )
	{
		if ( ! empty( $json['reason'] ) )
		{
			// Add "demonstration" so it appears in cron email...
			error_log( 'ipgeolocation.io error: ' . $json['reason'] . ' - IP: ' . $ip . ' demonstration' );
		}

		return '';
	}

	$country = $json['country_name'];

	return $country;
}
