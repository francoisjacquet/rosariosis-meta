#!/bin/bash

printf "Did you test new/modified SQL queries on both MySQL and PostgreSQL? (y/n) "
read TEST_SQL

if [ "$TEST_SQL" = "n" ]; then
	echo "Do some testing and come back..."

	exit 0
fi

printf "Current version: "
read CURRENT_VERSION

printf "New version: "
read VERSION

echo "Upgrading..."

cd ..

cp Warehouse.php Warehouse.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" Warehouse.tmp > Warehouse.php
rm Warehouse.tmp

cp rosariosis.sql rosariosis.tmp
sed "s/'$CURRENT_VERSION'/'$VERSION'/" rosariosis.tmp > rosariosis.sql
rm rosariosis.tmp

cp rosariosis_mysql.sql rosariosis_mysql.tmp
sed "s/'$CURRENT_VERSION'/'$VERSION'/" rosariosis_mysql.tmp > rosariosis_mysql.sql
rm rosariosis_mysql.tmp

cp COPYRIGHT COPYRIGHT.tmp
sed "s/version $CURRENT_VERSION/version $VERSION/" COPYRIGHT.tmp > COPYRIGHT
rm COPYRIGHT.tmp

# Line 3: upgrade "Month, YYYY" after dash -.
cp COPYRIGHT COPYRIGHT.tmp
sed "3!b;s/- .*/- $(date +%B), $(date +%Y)/" COPYRIGHT.tmp > COPYRIGHT
rm COPYRIGHT.tmp

cd ProgramFunctions/

cp Update.fnc.php Update.fnc.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" Update.fnc.tmp > Update.fnc.php
rm Update.fnc.tmp

#echo "Generate PDF doc..."

cd ../meta/

#php MarkDownPDFDoc.php > /dev/null 2>&1

echo "Complete!"

printf "Run tests? (y/n) "
read RUN_TESTS

if [ "$RUN_TESTS" = "y" ]; then
	bash tests-run.sh
fi

printf "Run QA? (y/n) "
read RUN_QA

if [ "$RUN_QA" = "y" ]; then
	bash qa-run.sh
fi

