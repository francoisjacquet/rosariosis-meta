<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Set\ValueObject\SetList;
use Rector\Set\ValueObject\DowngradeLevelSetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Utils\Rector\Rector\LongArrayToShortArrayRector;

return static function (ContainerConfigurator $containerConfigurator): void {
    // get parameters
    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::PATHS, [
        // __DIR__ . '/../../functions',
        // __DIR__ . '/../../classes/core',
        __DIR__ . '/../../ProgramFunctions',
        // __DIR__ . '/../../modules',
        // __DIR__ . '/../../plugins',
    ]);

    // is there a file you need to skip?
    $parameters->set(Option::SKIP, [
        // single file
        //  __DIR__ . '/src/ComplicatedFile.php',
        // or directory
        // __DIR__ . '/src',
        // or fnmatch
        __DIR__ . '/../../ProgramFunctions/Update.fnc.php',
        __DIR__ . '/../../modules/*/classes/*',
        __DIR__ . '/../../modules/*/vendor/*',
        __DIR__ . '/../../modules/*/includes/gateways/*', // SMS + SMS Premium.
        __DIR__ . '/../../plugins/*/classes/*',
        __DIR__ . '/../../plugins/*/vendor/*',
        __DIR__ . '/../../plugins/REST_API/api.php',

        // is there single rule you don't like from a set you use?
        // SimplifyIfReturnBoolRector::class,

        // or just skip rule in specific directory
        /*SimplifyIfReturnBoolRector::class => [
            // single file
            __DIR__ . '/src/ComplicatedFile.php',
            // or directory
            __DIR__ . '/src',
        ],*/
    ]);

    // Define what rule sets will be applied
    // @link https://github.com/rectorphp/rector/blob/4dc2c773aafe9a571b73e96def9a3f88eb851083/packages/Set/ValueObject/SetList.php
    // DowngradeLevelSetList:
    // @link https://github.com/rectorphp/rector/blob/66c0358afd2f7fa1fc1f2ed87b35309f7515c4b6/packages/Set/ValueObject/DowngradeLevelSetList.php
    // @link https://getrector.org/blog/how-all-frameworks-can-bump-to-php-81-and-you-can-use-older-php
    $containerConfigurator->import(SetList::DEAD_CODE);
    // CODE_QUALITY: NOPE, not interesting... longer & wrong code (require_once __DIR__)
    // $containerConfigurator->import(SetList::CODE_QUALITY);

    // get services (needed for register a single rule)
    $services = $containerConfigurator->services();

    // register a single rule
    // $services->set(LongArrayToShortArrayRector::class); // NOPE, bad results, use esc directly!
};
